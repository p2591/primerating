// import TelegramIcon from 'assets/icons/telegram.svg';
import Link from 'next/link';

// console.log(TelegramIcon);

function Nav({ children }: any) {
  return <div className="flex flex-1 justify-end space-x-10">{children}</div>;
}

Nav.Link = function NavLink({ href = '', children, ...props }: any) {
  return (
    <Link href={href}>
      <a
        {...props}
        className="text-base font-medium text-black hover:border-none hover:text-red-500"
      >
        {children}
      </a>
    </Link>
  );
};

function FooterLink({ href = '', children }: any) {
  return (
    <Link href={href}>
      <a
        target={'_blank'}
        className="border-none text-gray-500 hover:text-red-100"
      >
        {children}
      </a>
    </Link>
  );
}

function Footer() {
  return (
    <section className="bg-gradient-to-br from-[#15062d] to-[#2d0620] px-4 py-8 text-white sm:px-6 lg:pt-16">
      <div className={'mx-auto max-w-7xl'}>
        <article className="mb-10 justify-between py-3 md:space-x-36 lg:flex">
          <div className={'mb-10 lg:mb-0 lg:w-1/2'}>
            <h3 className="mb-4 bg-gradient-to-tr from-[#a258a7] to-[#ff497a] bg-clip-text text-3xl font-semibold text-transparent">
              Subscribe to Prime Times
            </h3>
            <p className={'mb-10 w-3/5 text-[#b7b3e5]'}>
              Our newsletter covering the latest developments in and around
              PrimeDAO
            </p>
            <form action="#">
              <input
                className={
                  'mr-2 w-3/5 rounded-full bg-gray-700 px-7 py-2 text-sm placeholder:text-gray-500'
                }
                type="email"
                placeholder={'Email'}
              />
              <button
                className={
                  'rounded-full bg-white py-2 px-6 text-sm font-semibold text-red-500 hover:bg-pink-400 hover:text-white'
                }
              >
                Sign up
              </button>
            </form>
          </div>
          <div className={'flex lg:w-1/2'}>
            <div className={'basis-1/2'}>
              <h4 className="mb-4 text-lg">Prime App</h4>
              <ul>
                <li className="my-2">
                  <FooterLink href="https://launch.prime.xyz/">
                    Prime Launch
                  </FooterLink>
                </li>
                <li className="my-2">
                  <FooterLink href="/pools">Prime Pools</FooterLink>
                </li>
                <li className="my-2">
                  <FooterLink href="/">Prime Rating</FooterLink>
                </li>
                <li className="my-2">
                  <FooterLink href="/deals">Prime Deals</FooterLink>
                </li>
              </ul>
            </div>
            <div className={'basis-1/2'}>
              <h4 className="mb-4 text-lg">PrimeDAO</h4>
              <ul>
                <li className="my-2">
                  <FooterLink href="https://app.boardroom.info/primedao/overview">
                    Vote
                  </FooterLink>
                </li>
                <li className="my-2">
                  <FooterLink href={'/dao'}>Dao</FooterLink>
                </li>
                <li className="my-2">
                  <FooterLink href={'https://medium.com/primedao'}>
                    Blog
                  </FooterLink>
                </li>
                <li className="my-2">
                  <FooterLink href="/brand">Brand Assets</FooterLink>
                </li>
              </ul>
            </div>
          </div>
        </article>
        <article className="hidden justify-between border-t border-gray-800 py-8 text-sm text-gray-600 md:block lg:flex">
          <div className="flex space-x-3 invert">
            <Link href="https://twitter.com/PrimeDAO">
              <a
                target="_blank"
                className="inline-flex items-center border-none align-middle text-[#0a070a] opacity-50 transition duration-200 ease-out hover:opacity-100"
                rel="noreferrer"
              >
                <svg
                  className="mr-2 h-5 w-5"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                  aria-hidden="true"
                >
                  <path d="M8.29 20.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0022 5.92a8.19 8.19 0 01-2.357.646 4.118 4.118 0 001.804-2.27 8.224 8.224 0 01-2.605.996 4.107 4.107 0 00-6.993 3.743 11.65 11.65 0 01-8.457-4.287 4.106 4.106 0 001.27 5.477A4.072 4.072 0 012.8 9.713v.052a4.105 4.105 0 003.292 4.022 4.095 4.095 0 01-1.853.07 4.108 4.108 0 003.834 2.85A8.233 8.233 0 012 18.407a11.616 11.616 0 006.29 1.84"></path>
                </svg>
                <div className="text-sm">Twitter</div>
              </a>
            </Link>
            <Link href="https://discord.gg/primedao">
              <a
                target="_blank"
                className="flex items-center border-none align-middle text-[#0a070a] opacity-50 transition duration-200 ease-out hover:opacity-100"
                rel="noreferrer"
              >
                <svg
                  className="mr-2 h-5 w-5"
                  aria-hidden="true"
                  focusable="false"
                  viewBox="0 0 640 512"
                >
                  <path
                    fill="currentColor"
                    d="M524.531,69.836a1.5,1.5,0,0,0-.764-.7A485.065,485.065,0,0,0,404.081,32.03a1.816,1.816,0,0,0-1.923.91,337.461,337.461,0,0,0-14.9,30.6,447.848,447.848,0,0,0-134.426,0,309.541,309.541,0,0,0-15.135-30.6,1.89,1.89,0,0,0-1.924-.91A483.689,483.689,0,0,0,116.085,69.137a1.712,1.712,0,0,0-.788.676C39.068,183.651,18.186,294.69,28.43,404.354a2.016,2.016,0,0,0,.765,1.375A487.666,487.666,0,0,0,176.02,479.918a1.9,1.9,0,0,0,2.063-.676A348.2,348.2,0,0,0,208.12,430.4a1.86,1.86,0,0,0-1.019-2.588,321.173,321.173,0,0,1-45.868-21.853,1.885,1.885,0,0,1-.185-3.126c3.082-2.309,6.166-4.711,9.109-7.137a1.819,1.819,0,0,1,1.9-.256c96.229,43.917,200.41,43.917,295.5,0a1.812,1.812,0,0,1,1.924.233c2.944,2.426,6.027,4.851,9.132,7.16a1.884,1.884,0,0,1-.162,3.126,301.407,301.407,0,0,1-45.89,21.83,1.875,1.875,0,0,0-1,2.611,391.055,391.055,0,0,0,30.014,48.815,1.864,1.864,0,0,0,2.063.7A486.048,486.048,0,0,0,610.7,405.729a1.882,1.882,0,0,0,.765-1.352C623.729,277.594,590.933,167.465,524.531,69.836ZM222.491,337.58c-28.972,0-52.844-26.587-52.844-59.239S193.056,219.1,222.491,219.1c29.665,0,53.306,26.82,52.843,59.239C275.334,310.993,251.924,337.58,222.491,337.58Zm195.38,0c-28.971,0-52.843-26.587-52.843-59.239S388.437,219.1,417.871,219.1c29.667,0,53.307,26.82,52.844,59.239C470.715,310.993,447.538,337.58,417.871,337.58Z"
                  ></path>
                </svg>
                <div className="text-sm">Discord</div>
              </a>
            </Link>
            <Link href="https://medium.com/primedao">
              <a
                target="_blank"
                className="flex items-center border-none align-middle text-[#0a070a] opacity-50 transition duration-200 ease-out hover:opacity-100"
                rel="noreferrer"
              >
                <img
                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAYAAACpF6WWAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAADzSURBVHgB1VNtEcIwDA0cAiZhDsABSJiDHQ5wwCTggDlgU7CiABxsDoaD0dy1EHJp2vFv7y7XfL60uQZgKVhZ6ZT4YOUo+K9WcqUOpojw4jxWs4Y4CmYfIAGY9FA68/F0Sq4B0rRiwZHpmcvLiL8XSJEHQs9viY5EO/IqDxOoDZI2zC7YKeVESY2VF7FLd+7dibEWZpJi0ZPYOIIzfL/XHRRoX4rf5ET0G/xJWjM7Uxomk+IIjOA38DvvWaQIaXY1ROBJ80C8SWzk8RmRtKJ0NXvBX4K8rqPPCe2xx4X4/A+olLppA8q6OeAItkRHDAl1C8AbicZq3eCYo3kAAAAASUVORK5CYII="
                  alt=""
                  className="mr-2 h-4 w-4"
                />
                <div className="text-sm">Medium</div>
              </a>
            </Link>
            <Link href="https://t.me/primedao">
              <a
                target="_blank"
                className="flex items-center border-none align-middle text-[#0a070a] opacity-50 transition duration-200 ease-out hover:opacity-100"
                rel="noreferrer"
              >
                <img
                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAYAAACpF6WWAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAD1SURBVHgBtZRvDYMwEMXfkgmYBCQgoQ6GA+qAOWAScDAcbA6QQKYACUjorkmb3ro2vRF4yftAaX59d/0DHKAT9tOF3JJr7CBFfpBXsiFPZ2yTTdWRm0SyN/6UsklcopxvEpBN1SOU522/n+QlGlfYmMqON4mFjAvxpTqTiqdr3IKpOTMvr0G5VwPCscnNeXloCTYj9KktzL17aEXW+G24LY/vZFcAZjdJMzg/f70AaFzArFZWihS4oiC7KRNbQAKdSlDFyjFCDyXoBeHGxCXmoFcINEYw5ZKPGajoydMIvaoS/5YIKpJ/3nKqEFLP2Fka7CYdpg+wdLDpzlx6BwAAAABJRU5ErkJggg=="
                  alt=""
                  className="mr-2 h-4 w-4"
                />
                <div className="text-sm">Telegram</div>
              </a>
            </Link>
            <Link href="https://forum.prime.xyz/">
              <a
                target="_blank"
                className="flex items-center border-none align-middle text-[#0a070a] opacity-50 transition duration-200 ease-out hover:opacity-100"
                rel="noreferrer"
              >
                <img
                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAYAAACpF6WWAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAABkSURBVHgB7dVBDYAwEETRDwqQUCmVhAQkVBIScAAOkAAl7SZwJJm97Usmvf3jFhwM/c19CptFz7oJEYteCI04iGhEI+oRXdE5+CnVFdoBem9BYKadS4tmRFLdro6a4hF9fH6NGzc6EPETsk41AAAAAElFTkSuQmCC"
                  alt=""
                  className="mr-2 h-4 w-4"
                />
                <div className="text-sm">Forum</div>
              </a>
            </Link>
            <Link href="https://app.boardroom.info/primedao/overview">
              <a
                target="_blank"
                className="flex items-center border-none align-middle text-[#0a070a] opacity-50 transition duration-200 ease-out hover:opacity-100"
                rel="noreferrer"
              >
                <img
                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAOCAYAAAAbvf3sAAAAAXNSR0IArs4c6QAAAOZlWElmTU0AKgAAAAgABgESAAMAAAABAAEAAAEaAAUAAAABAAAAVgEbAAUAAAABAAAAXgExAAIAAAAhAAAAZgEyAAIAAAAUAAAAiIdpAAQAAAABAAAAnAAAAAAAAABIAAAAAQAAAEgAAAABQWRvYmUgUGhvdG9zaG9wIDIyLjQgKE1hY2ludG9zaCkAADIwMjE6MDU6MjggMTE6MzQ6MjgAAASQBAACAAAAFAAAANKgAQADAAAAAQABAACgAgAEAAAAAQAAAAygAwAEAAAAAQAAAA4AAAAAMjAyMTowNToyOCAxMToyNDo0MADnoEyqAAAACXBIWXMAAAsTAAALEwEAmpwYAAALKWlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNi4wLjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgICAgICAgICAgIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIKICAgICAgICAgICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iCiAgICAgICAgICAgIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiCiAgICAgICAgICAgIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iPgogICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3BuZzwvZGM6Zm9ybWF0PgogICAgICAgICA8eG1wOk1vZGlmeURhdGU+MjAyMS0wNS0yOFQxMTozNDoyOCswMjowMDwveG1wOk1vZGlmeURhdGU+CiAgICAgICAgIDx4bXA6Q3JlYXRvclRvb2w+QWRvYmUgUGhvdG9zaG9wIDIyLjQgKE1hY2ludG9zaCk8L3htcDpDcmVhdG9yVG9vbD4KICAgICAgICAgPHhtcDpDcmVhdGVEYXRlPjIwMjEtMDUtMjhUMTE6MjQ6NDArMDI6MDA8L3htcDpDcmVhdGVEYXRlPgogICAgICAgICA8eG1wOk1ldGFkYXRhRGF0ZT4yMDIxLTA1LTI4VDExOjM0OjI4KzAyOjAwPC94bXA6TWV0YWRhdGFEYXRlPgogICAgICAgICA8eG1wTU06SGlzdG9yeT4KICAgICAgICAgICAgPHJkZjpTZXE+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6c29mdHdhcmVBZ2VudD5BZG9iZSBQaG90b3Nob3AgMjIuNCAoTWFjaW50b3NoKTwvc3RFdnQ6c29mdHdhcmVBZ2VudD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OndoZW4+MjAyMS0wNS0yOFQxMToyNDo0MCswMjowMDwvc3RFdnQ6d2hlbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0Omluc3RhbmNlSUQ+eG1wLmlpZDphNjA5Mjc5ZS1iZTNhLTRjNzAtYTgwNS05ZjU1NTQzMDM3OTA8L3N0RXZ0Omluc3RhbmNlSUQ+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDphY3Rpb24+Y3JlYXRlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpzb2Z0d2FyZUFnZW50PkFkb2JlIFBob3Rvc2hvcCAyMi40IChNYWNpbnRvc2gpPC9zdEV2dDpzb2Z0d2FyZUFnZW50PgogICAgICAgICAgICAgICAgICA8c3RFdnQ6Y2hhbmdlZD4vPC9zdEV2dDpjaGFuZ2VkPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6d2hlbj4yMDIxLTA1LTI4VDExOjI1OjA2KzAyOjAwPC9zdEV2dDp3aGVuPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6aW5zdGFuY2VJRD54bXAuaWlkOjA2Nzg5OTkxLTlmNWMtNGJkNC1hNWQxLWNkYjg5YzE1NmFiMjwvc3RFdnQ6aW5zdGFuY2VJRD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5zYXZlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpzb2Z0d2FyZUFnZW50PkFkb2JlIFBob3Rvc2hvcCAyMi40IChNYWNpbnRvc2gpPC9zdEV2dDpzb2Z0d2FyZUFnZW50PgogICAgICAgICAgICAgICAgICA8c3RFdnQ6Y2hhbmdlZD4vPC9zdEV2dDpjaGFuZ2VkPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6d2hlbj4yMDIxLTA1LTI4VDExOjM0OjI4KzAyOjAwPC9zdEV2dDp3aGVuPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6aW5zdGFuY2VJRD54bXAuaWlkOmUyNjhlN2ZmLTg1ZTAtNGUyMC1hNjYwLTExMTc5ZWM0MDAxZDwvc3RFdnQ6aW5zdGFuY2VJRD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5zYXZlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgPC9yZGY6U2VxPgogICAgICAgICA8L3htcE1NOkhpc3Rvcnk+CiAgICAgICAgIDx4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ+eG1wLmRpZDphNjA5Mjc5ZS1iZTNhLTRjNzAtYTgwNS05ZjU1NTQzMDM3OTA8L3htcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD4KICAgICAgICAgPHhtcE1NOkRvY3VtZW50SUQ+eG1wLmRpZDphNjA5Mjc5ZS1iZTNhLTRjNzAtYTgwNS05ZjU1NTQzMDM3OTA8L3htcE1NOkRvY3VtZW50SUQ+CiAgICAgICAgIDx4bXBNTTpJbnN0YW5jZUlEPnhtcC5paWQ6ZTI2OGU3ZmYtODVlMC00ZTIwLWE2NjAtMTExNzllYzQwMDFkPC94bXBNTTpJbnN0YW5jZUlEPgogICAgICAgICA8cGhvdG9zaG9wOklDQ1Byb2ZpbGU+RG90IEdhaW4gMjAlPC9waG90b3Nob3A6SUNDUHJvZmlsZT4KICAgICAgICAgPHBob3Rvc2hvcDpDb2xvck1vZGU+MTwvcGhvdG9zaG9wOkNvbG9yTW9kZT4KICAgICAgICAgPHRpZmY6T3JpZW50YXRpb24+MTwvdGlmZjpPcmllbnRhdGlvbj4KICAgICAgICAgPHRpZmY6WFJlc29sdXRpb24+NzI8L3RpZmY6WFJlc29sdXRpb24+CiAgICAgICAgIDx0aWZmOllSZXNvbHV0aW9uPjcyPC90aWZmOllSZXNvbHV0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KdexIqgAAAVhJREFUKBVtUrtKxFAQzftF3k3CkuC6RLZItyD+wFYi/oC/Y2shipVYWFpZ2fgN/oTbBBLYQlyWZOF6Jt57ieDAcM7MnLmvuYryv5kinWXZerlcznhsiPwULRF4nncNf+SxBlRFTaAU27b9oOs6S9O05kVPiATanKiWZT2rqsqALzwXAV24PJJDhcViEUH0Bso0TeuKojih/NSog3xfluVss9m8DsNwSgI0NNvt9gh0bhjGAZYEQdBSTYmiaAXBJyiD9wJxLOLfFOM+HXRrJUmSc9M0v+jMaBrgDHzgTbQAiZswDM/AFQOv0e12uyvf9zvEqOk64pu+71ckwGIfcRxftG3bIKSn/Wt5ns+xyx5ZBvF7VVUhV9Arygaa7Bg4jkODYsAnoBiUnA9yY3IU4xsc40UYjnlPBW7ym4iEWEXBPW5d170TBaAc1CT3S+u6TrHD5aSgT7ikP3wTSyxW8QZdAAAAAElFTkSuQmCC"
                  alt=""
                  className="mr-2 h-4 w-4 "
                />
                <div className="text-sm">Govern</div>
              </a>
            </Link>
          </div>
          <div className={'py-5 text-center text-sm lg:py-0'}>
            <a
              href={`assets/pdf/privacy.pdf`}
              target="_blank"
              className={
                'border-none font-bold text-[#8668fc] hover:text-[#ff497a]'
              }
              rel="noreferrer"
            >
              Privacy
            </a>{' '}
            &amp; {` `}
            <a
              href={`assets/pdf/Cookies.pdf`}
              target="_blank"
              className={
                'border-none font-bold text-[#8668fc] hover:text-[#ff497a]'
              }
              rel="noreferrer"
            >
              Cookies
            </a>
            • Terms of Use
          </div>
          <div className="text-center lg:order-first">
            Prime Development Foundation © 2021
          </div>
        </article>
      </div>
    </section>
  );
}

export { Footer };
