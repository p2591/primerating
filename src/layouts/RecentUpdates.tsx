import NewListings from '@/components/NewListings';
import RatingAPI from '@/components/RatingAPI';
import UpdatesAd from '@/components/UpdatesAd';

function RecentUpdates() {
  return (
    <div className={'grid grid-cols-1 gap-4 md:grid-cols-3'}>
      <RatingAPI />
      <NewListings />
      <UpdatesAd />
    </div>
  );
}

export default RecentUpdates;
