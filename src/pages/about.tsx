import Image from 'next/image';
import Link from 'next/link';

import PrimeFeatures from '@/components/PrimeFeatures';
import { Meta } from '@/layouts/Meta';

function Button({
  href = '',
  className = '',
  active = false,
  children,
  ...props
}: any) {
  return (
    <Link href={href}>
      <a
        className={`inline-flex items-center rounded-full border hover:border hover:border-transparent ${
          active
            ? 'border-pink-200 bg-pink-50 text-red-500 hover:bg-pink-500 hover:text-white'
            : 'bg-pink-600 text-white hover:border-pink-200 hover:bg-white hover:text-red-500'
        } px-5 py-3 text-sm font-bold uppercase ${className}`}
        {...props}
      >
        {children}
      </a>
    </Link>
  );
}

const About = () => (
  <section className="w-full px-1 text-gray-700 antialiased">
    <Meta title="About" description="" />

    <article className="bg-white bg-[image:url('/assets/about/animated.gif')] bg-cover bg-scroll bg-no-repeat ">
      <div className="mx-auto max-w-screen-md py-20 px-5 text-xl md:py-56">
        <h2
          className={
            'bg-gradient-to-tr from-[#a258a7] to-[#ff497a] bg-clip-text text-4xl font-bold leading-tight text-transparent md:text-6xl'
          }
        >
          Crowd wisdom for protocol transparency.
        </h2>
        <p className={'mb-10 md:mb-16'}>
          A permissionless framework for quantifying quality and risk of open
          finance protocols. Prime Rating is a public good for the systemic
          advancement of DeFi.
        </p>
        <div className={'flex space-x-3'}>
          <Button href={'/'}>RATING OVERVIEW</Button>
          <Button
            href="https://docs.prime.xyz/prime-rating/prime-rating-squad/master"
            target="_blank"
            active={true}
          >
            Learn more
          </Button>
        </div>
      </div>
    </article>

    <article
      className={'content mx-auto max-w-screen-lg px-5 pt-10 text-xl md:pt-56'}
    >
      <ul>
        <li className="mb-20 flex flex-col space-x-3 md:flex-row">
          <div className={'flex basis-1/2 items-center justify-center '}>
            <Image
              width={558}
              height={416}
              src={'/assets/about/Prime-Rating.png'}
              alt=""
            />
          </div>
          <div className={'basis-1/2 text-base'}>
            <p>NEUTRAL BENCHMARKING</p>

            <h2
              className={
                'mb-10 text-4xl font-bold leading-10 text-black md:text-5xl'
              }
            >
              Improving the resiliency of DeFi
            </h2>
            <p>
              Prime Rating offers a <b>neutral benchmark</b> in the form of a
              rating from A+ to D.{' '}
              <b>The score represents the quality and fundamental evaluation</b>
              of a DeFi protocol, all based on publicly available data. Our Beta
              version <b>assesses</b> protocols from the perspective of a token
              holder, based on{' '}
              <b>fundamental analysis and technical quality.</b>
            </p>

            <p>
              There are already <b>70+</b> reports available.{' '}
              <b>This data is valuable information</b> for decentralized
              applications, such as the Prime DeFi dHedge pool with over 125k
              AuM, <b>with more products coming up.</b>
            </p>
          </div>
        </li>
        <li className="mb-20 flex flex-col space-x-3 md:flex-row">
          <div className={'basis-1/2 text-base'}>
            <p>BECOME A PRIME RATER</p>

            <h2
              className={
                'mb-10 text-4xl font-bold leading-10 text-black md:text-5xl'
              }
            >
              Get rewarded
            </h2>
            <p>
              The Prime Rating process enables anyone to create and submit a
              rating report that will update the <b>rating score</b> if accepted
              by governance.{' '}
              <b>To get involved, join our Discord and read our guidelines.</b>
            </p>
            <p
              className={
                'flex flex-col space-y-3 pt-10 md:flex-row md:space-y-0 md:space-x-3'
              }
            >
              <Button
                active
                target={'_blank'}
                href={`https://docs.prime.xyz/prime-rating/products/permissionless-rating`}
              >
                Learn more
              </Button>
              <Button href={'http://discord.gg/primedao'} target={'_blank'}>
                Join the discord
                <svg
                  className={'ml-2 h-4 w-3'}
                  fill="currentColor"
                  viewBox="0 0 46.001 85.999"
                >
                  <path d="m1.003 80.094c-1.338 1.352-1.338 3.541 0 4.893 1.337 1.35 3.506 1.352 4.845 0l39.149-39.539c1.338-1.352 1.338-3.543 0-4.895l-39.149-39.539c-1.339-1.352-3.506-1.352-4.845 0-1.338 1.352-1.338 3.541-.001 4.893l35.704 37.093z" />
                </svg>
              </Button>
            </p>
          </div>
          <div className={'flex basis-1/2 items-center justify-center '}>
            <Image
              width={465}
              height={262}
              src={'/assets/about/rating-POAPS.png'}
              alt=""
            />
          </div>
        </li>
        <li className="mb-20 flex flex-col text-base md:flex-row md:space-x-16">
          <div className={''}>
            <Image
              width={43}
              height={45}
              src="/assets/icons/prime-checkmark.svg"
              alt=""
            />
            <h3 className={'text-xl font-bold text-black'}>
              Fundamental Review
            </h3>
            <p>
              The Fundamental Analysis evaluates the overall quality of a
              project based on its Value Proposition, Tokenomics, Team,
              Governance, and Regulatory qualities.
            </p>
          </div>
          <div className={''}>
            <Image
              width={43}
              height={45}
              src="/assets/icons/prime-checkmark.svg"
              alt=""
            />
            <h3 className={'text-xl font-bold text-black'}>Technical Review</h3>
            <p>
              Created in collaboration with{' '}
              <a
                target="_blank"
                className={
                  'border-none font-bold text-[#8668fc] hover:text-[#ff497a]'
                }
                href="https://defisafety.com/"
                rel="noreferrer"
              >
                DeFi Safety
              </a>
              , the Technical quality Review evaluates the technical quality of
              a project based on it&apos;s Code, Documentation, Testing,
              Security and Access Controls.
            </p>
          </div>
        </li>
        <li className="mb-20 text-base">
          <div className="mb-8 text-center">
            <h3 className={'text-sm font-semibold uppercase text-red-500'}>
              IN COLLABORATION WITH
            </h3>
          </div>

          <section className="lg:my-12 lg:flex lg:justify-center">
            <div className="bg-[#15062d] lg:flex lg:rounded-lg lg:shadow-lg">
              <div className="bg-white p-6 lg:w-1/3 lg:rounded-l-lg">
                <h2 className="text-2xl font-bold text-black md:text-4xl">
                  DeFi Safety
                </h2>
                <p className="mt-4 text-gray-600">
                  DeFi Safety rates products on software quality and use of best
                  practices. In DeFi, you have to trust the software as there is
                  nothing else backing you up.
                </p>

                <div className="mt-8">
                  <a
                    href="https://www.defisafety.com/"
                    target={'_blank'}
                    className="border-none py-2 font-semibold text-[#8668fc] hover:text-[#ff497a]"
                    rel="noreferrer"
                  >
                    Website &rarr;
                  </a>
                </div>
              </div>
              <div className="lg:w-3/4">
                <div
                  className="h-72 bg-contain bg-center bg-no-repeat lg:h-full lg:rounded-lg"
                  style={{
                    backgroundImage: `url('/assets/about/defisafety.png')`,
                  }}
                />
              </div>
            </div>
          </section>

          <section className="lg:my-12 lg:flex lg:justify-center">
            <div className="bg-[#15062d] lg:flex lg:rounded-lg lg:shadow-lg">
              <div className="bg-white p-6 lg:w-1/3 lg:rounded-l-lg">
                <h2 className="text-2xl font-bold text-black md:text-4xl">
                  Metaportal
                </h2>
                <p className="mt-4 text-gray-600">
                  Fueled by diverse revenue streams and an intrinsic curiosity,
                  MetaPortal aims to contribute to the development of the open
                  metaverse through its various projects and initiatives.
                </p>

                <div className="mt-8">
                  <a
                    href="https://www.metaportal.wtf/"
                    target={'_blank'}
                    className="border-none py-2 font-semibold text-[#8668fc] hover:text-[#ff497a]"
                    rel="noreferrer"
                  >
                    Website &rarr;
                  </a>
                </div>
              </div>
              <div className="lg:w-3/4">
                <div
                  className="h-72 bg-cover bg-center bg-no-repeat lg:h-full lg:rounded-lg"
                  style={{
                    backgroundImage: `url('/assets/about/metaportal.png')`,
                  }}
                />
              </div>
            </div>
          </section>
        </li>
      </ul>
    </article>
    <PrimeFeatures />
  </section>
);

export default About;
