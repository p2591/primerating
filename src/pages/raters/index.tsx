import { useCallback, useState } from 'react';

import { getProjects } from '@/api/projects';
import { getRaters, searchRaters } from '@/api/raters';
import RatersTable from '@/components/ContentTables/RatersTable';
import SearchInput from '@/components/SearchInput';
import Tabs, { Tab } from '@/components/Tabs';
import { Meta } from '@/layouts/Meta';
import { Main } from '@/templates/Main';
import { throttle } from '@/utils';

function Raters({ initialData }: any) {
  const [data, setData]: any = useState(initialData);
  const raters = data; // ?.content ?? [];

  const fetchData = useCallback(async (params: any = {}) => {
    const { sector: newSector, ...restParams } = params;
    try {
      const { data: updatedData } = await getProjects(newSector, restParams);
      setData(updatedData);
    } catch (error) {
      console.log('cannot get projects', error);
    }
  }, []);

  async function searchHandler(value: string) {
    if (!value) {
      fetchData();
      return;
    }
    try {
      const { data: updatedData } = await searchRaters(value);
      setData(updatedData);
    } catch (error: any) {
      console.log('error', error);
    }
  }

  // @ts-ignore
  const throttledSearchProjects = throttle(searchHandler, 250);

  return (
    <Main meta={<Meta title="Raters" description="" />}>
      <section
        className={
          'mb-10 max-w-screen-lg rounded-xl bg-white py-6 text-xs text-black '
        }
      >
        <article
          className={
            'flex w-full shrink-0 flex-col overflow-auto md:overflow-visible'
          }
        >
          <div
            className={
              'relative top-0 flex grow flex-col bg-white   md:sticky md:top-[4.9rem]'
            }
          >
            <div className={`mb-4 flex flex-row justify-between px-3 md:px-6`}>
              <Tabs>
                {() => (
                  <>
                    <Tab active={true}>Raters</Tab>
                  </>
                )}
              </Tabs>
              <div className={'my-5 flex min-h-[50px] items-center md:my-0'}>
                <SearchInput
                  onChange={throttledSearchProjects}
                  className={'hidden'}
                />
              </div>
            </div>
          </div>
          <div className="mb-5">
            <RatersTable data={raters} />
          </div>
        </article>
      </section>
    </Main>
  );
}

export async function getServerSideProps(props: any) {
  const { ...params } = props.query;
  // Fetch data from external API
  const res = await getRaters(params);
  const initialData = res?.data?._embedded.userDtoes;
  // Pass data to the page via props
  return { props: { initialData } };
}

export default Raters;
