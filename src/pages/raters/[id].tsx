import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useMemo, useState } from 'react';
import toast from 'react-hot-toast';
import slugify from 'slugify';

import { getRaterById } from '@/api/raters';
import Avatar from '@/components/Avatar';
import BackButton from '@/components/Button/BackButton';
import { groupBy } from '@/helpers';
import { formatDate } from '@/helpers/date';
import { convertToBase64 } from '@/helpers/logo';
import { Meta } from '@/layouts/Meta';
import { Main } from '@/templates/Main';

function Rater({ id }: any) {
  const router = useRouter();
  const [user, setData]: any = useState({});
  const slugifiedRank = slugify(user.rank || '', { lower: true });

  /**
   * Group array of reports in 2 level group. By "sector" and by "type" then
   * @return { defi: { fundamentals: [...], technical: [...]}, ... }
   */
  const reports = useMemo(() => {
    const groupBySector = groupBy(
      user.reports || [],
      (item: any) => item.sector
    );

    groupBySector.forEach((values: any, sector: any) => {
      const groupByType = groupBy(values, (item: any) => item.type);
      groupBySector.set(sector, groupByType);
    });

    return groupBySector;
  }, [user]);

  async function fetchUser(userId: any) {
    try {
      const { data: updatedData } = await getRaterById(userId);
      setData(updatedData);
    } catch (error: any) {
      toast.error(error?.message);
    }
  }

  useEffect(() => {
    if (id) {
      fetchUser(id);
    }
  }, [id]);

  return (
    <Main meta={<Meta title="Raters" description="" />}>
      <section
        className={
          'my-10 mx-auto max-w-screen-lg rounded-xl bg-white p-4 text-xs md:p-8 md:text-base'
        }
      >
        <article className={'mb-6 flex flex-col md:flex-row'}>
          <div className={'grow'}>
            <div className="mb-10 flex">
              <Avatar
                src={convertToBase64(user.logo)}
                placeholder="/website/assets/placeholders/rater.png"
              />
              <div>
                <h3
                  className={
                    'mb-1 text-xl font-bold leading-none text-black md:text-3xl'
                  }
                >
                  {user.name}
                </h3>
                <p className={'text-base capitalize'}>{user.rank}</p>
              </div>
            </div>
            {user.twitter && (
              <div className={'mb-8'}>
                <span className={'text-sm text-gray-800'}>Twitter</span>
                <div>
                  <Link href={`https://twitter.com/${user.twitter}`}>
                    <a
                      target={'_blank'}
                      className={
                        'border-none font-bold text-[#8668fc] hover:text-[#ff497a]'
                      }
                    >
                      @${user.twitter}
                    </a>
                  </Link>
                </div>
              </div>
            )}
            <div className={'pr-0 text-gray-500 md:pr-20'}>
              {user.description}
            </div>
          </div>
          <div
            className={`score-card mx-auto my-5 w-[240px] min-w-[240px] rounded-xl pt-4 pb-8 text-center md:my-0 ${slugifiedRank} px-6`}
          >
            <img
              className={'mx-auto mb-5 inline-block'}
              src={`${
                router.basePath
              }/assets/ranks/${user.rank?.toLowerCase()}.png`}
              alt=""
              width={200}
            />
            <div
              className={`flex justify-center space-x-2 text-center text-2xl font-bold leading-6 text-[#0a070a]`}
            >
              <h4 className={''}>{user.rank}</h4>
              <span className={'font-normal text-[rgb(152,151,155)]/20'}>
                |
              </span>
              <span className={''}>{user.rxp}</span>
            </div>
            <p className={'text-base'}>
              Ranking #<b className={'ml-1 text-xl'}>{user.ranking}</b>
            </p>
          </div>
        </article>

        <article
          className={'mb-6 grid grid-cols-1 gap-2 md:grid-cols-2 md:gap-6'}
        >
          {/* @ts-ignore */}
          {[...reports.keys()].map((sector: any, index: number) => {
            const sectorReports = reports.get(sector);
            return [...sectorReports.keys()].map((type, typeIndex) => {
              const values = sectorReports.get(type);

              return (
                <div
                  key={`${sector}-${index}-${typeIndex}`}
                  className="w-full rounded-md bg-[#f9f6f9] p-4"
                >
                  <h3 className="mb-3 text-xl font-bold capitalize text-red-500">
                    {sector} {type} Reports
                  </h3>
                  <div>
                    <div className={'w-full table-auto text-base'}>
                      {[...values].map(({ project, acceptedAt }: any) => {
                        return (
                          <div
                            key={project.id}
                            className={
                              'flex justify-between border-none py-1 align-middle text-[#98979b]'
                            }
                          >
                            <Link href={`/projects/${project.id}`}>
                              <a
                                className={
                                  'border-none font-semibold text-[#98979b] hover:text-[#ff497a]'
                                }
                              >
                                <Avatar
                                  src={convertToBase64(project.logo)}
                                  placeholder="/website/assets/placeholders/project.png"
                                />
                                {project.name}
                              </a>
                            </Link>
                            <div className={'py-1 text-right'}>
                              {formatDate(acceptedAt)}
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              );
            });
          })}
        </article>
        <div className={'mt-10'}>
          <BackButton />
        </div>
      </section>
    </Main>
  );
}

export async function getStaticPaths() {
  return {
    paths: ['/raters/1'],
    fallback: true,
  };
}

export async function getStaticProps({ params }: any) {
  return {
    props: { id: params.id },
  };
}

// export async function getServerSideProps() {
//   return { props: {} };
// }

export default Rater;
