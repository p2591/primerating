import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useState } from 'react';

import { getProjects, searchProjects, SECTORS } from '@/api/projects';
import SectorTable from '@/components/ContentTables/DefiTable';
import FundamentalTable from '@/components/ContentTables/FundamentalTable';
import MetaverseTable from '@/components/ContentTables/MetaverseTable';
import TechnicalTable from '@/components/ContentTables/TechnicalTable';
import Disclaimer from '@/components/Disclaimer';
import SearchInput from '@/components/SearchInput';
import Pagination from '@/components/Table/Pagination';
import Tabs, { Tab } from '@/components/Tabs';
import { isEqual } from '@/helpers';
import { Meta } from '@/layouts/Meta';
import RecentUpdates from '@/layouts/RecentUpdates';
import { Main } from '@/templates/Main';
import { throttle } from '@/utils';

function Button({
  href = '',
  active = false,
  children,
  className = '',
  ...rest
}: any) {
  return (
    <Link href={href}>
      <a
        className={`rounded-full ${
          active ? 'bg-pink-200' : '  bg-gray-100 hover:bg-white'
        } border-2 border-pink-200 px-3 py-2 text-sm font-bold capitalize text-red-400 hover:border-transparent hover:text-red-500 ${className}`}
        {...rest}
      >
        {children}
      </a>
    </Link>
  );
}

function Projects({ initialData }: any) {
  const router = useRouter();
  const [prevParams, setPrevParams] = useState(router.query);
  const [sector, setSector]: any = useState(
    router.query?.sector || SECTORS.DEFI
  );
  const [data, setData] = useState(initialData);
  const projectsData = data?.content ?? [];
  const pageData = data?.pageable ?? {};
  const { pageNumber: number, pageSize: size } = pageData;

  const fetchData = useCallback(
    async (params: any = {}) => {
      const { sector: newSector, ...restParams } = params;

      try {
        const { data: updatedData } = await getProjects(newSector, restParams);
        setData(updatedData);
      } catch (error) {
        console.log('cannot get projects', error);
      }
    },
    [router.query]
  );

  useEffect(() => {
    if (!isEqual(prevParams, router.query)) {
      fetchData(router.query).then(() => {
        setPrevParams(router.query);
        setSector(router.query.sector || SECTORS.DEFI);
      });
    }
  }, [router.query]);

  const isBelongToDEFI =
    sector === SECTORS.DEFI ||
    sector === SECTORS.TECHNICAL ||
    sector === SECTORS.FUNDAMENTAL;

  async function onPageChange(toPage = 0) {
    const urlParams = new URLSearchParams(window.location.search);
    urlParams.set('page', `${toPage}`);
    urlParams.set('size', '100');

    const params = urlParams.toString();
    await router.push(`?${params}`);
  }

  async function searchHandler(value: string) {
    if (!value) {
      fetchData(router.query);
      return;
    }
    try {
      const { data: updatedData } = await searchProjects(sector, value);
      setData(updatedData);
    } catch (error: any) {
      console.log('error', error);
    }
  }

  const throttledSearchProjects = throttle(searchHandler, 250);

  return (
    <Main meta={<Meta title="Rating Defi" description="" />}>
      <article className={'mb-14'}>
        <RecentUpdates />
      </article>

      <article className="mb-6">
        <ul className="flex flex-wrap justify-center text-xl">
          <li className="m-2">
            <Button
              href={`/projects/?sector=${SECTORS.DEFI}`}
              active={isBelongToDEFI}
            >
              DeFi ratings
            </Button>
          </li>
          <li className="m-2">
            <Button
              href={`/projects/?sector=${SECTORS.METAVERSE}`}
              active={sector === SECTORS.METAVERSE}
            >
              Metaverse ratings
            </Button>
          </li>
          <li className="m-2">
            <Button
              href={`/projects/?sector=${SECTORS.REFI}`}
              active={sector === SECTORS.REFI}
            >
              ReFi ratings
            </Button>
          </li>
        </ul>
      </article>

      <Disclaimer />

      <section
        className={
          'mb-10 max-w-screen-lg rounded-xl bg-white py-6 text-xs text-black'
        }
      >
        <article
          className={
            'flex w-full shrink-0 flex-col overflow-auto md:overflow-visible'
          }
        >
          <div
            className={
              'relative top-0 flex grow flex-col bg-white md:sticky md:top-[4.9rem]'
            }
          >
            <div
              className={`mb-4 flex flex-row space-x-6 ${
                isBelongToDEFI ? 'justify-between' : 'justify-end'
              } px-3  md:px-6`}
            >
              {isBelongToDEFI && (
                <Tabs initialActive={router.query.sector || SECTORS.DEFI}>
                  {({ active, setActive }: any) => (
                    <>
                      <Tab
                        active={active === SECTORS.DEFI}
                        onClick={() => {
                          setActive(SECTORS.DEFI);
                          router.push(`?sector=${SECTORS.DEFI}`);
                        }}
                      >
                        Prime Ratings
                      </Tab>
                      <Tab
                        active={active === SECTORS.TECHNICAL}
                        onClick={() => {
                          setActive(SECTORS.TECHNICAL);
                          router.push(`?sector=${SECTORS.TECHNICAL}`);
                        }}
                      >
                        Technical
                      </Tab>
                      <Tab
                        active={active === SECTORS.FUNDAMENTAL}
                        onClick={() => {
                          setActive(SECTORS.FUNDAMENTAL);
                          router.push(`?sector=${SECTORS.FUNDAMENTAL}`);
                        }}
                      >
                        Fundamentals
                      </Tab>
                    </>
                  )}
                </Tabs>
              )}
              {sector !== SECTORS.TECHNICAL && sector !== SECTORS.FUNDAMENTAL && (
                <div className={'my-5 flex min-h-[50px] items-center md:my-0'}>
                  <SearchInput onChange={throttledSearchProjects} />
                </div>
              )}
            </div>
          </div>

          <div className="mb-5 ">
            {sector === SECTORS.TECHNICAL && (
              <TechnicalTable number={number} size={size} data={projectsData} />
            )}
            {sector === SECTORS.FUNDAMENTAL && (
              <FundamentalTable
                number={number}
                size={size}
                data={projectsData}
              />
            )}
            {sector === SECTORS.DEFI && (
              <SectorTable number={number} size={size} data={projectsData} />
            )}
            {sector === SECTORS.METAVERSE && (
              <MetaverseTable number={number} size={size} data={projectsData} />
            )}
          </div>
          {pageData && (
            <Pagination
              total={data?.totalPages}
              current={number}
              onClick={onPageChange}
            />
          )}
        </article>
      </section>
    </Main>
  );
}

// This gets called on every request
export async function getServerSideProps(props: any) {
  const { sector, ...params } = props.query;
  // Fetch data from external API
  const res = await getProjects(sector, params);
  const initialData = res?.data;

  // Pass data to the page via props
  return { props: { initialData, sector: sector || null } };
}

export default Projects;
