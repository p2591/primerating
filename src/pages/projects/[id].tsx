import Link from 'next/link';
import { useEffect, useState } from 'react';
import toast from 'react-hot-toast';

import { getProjectById } from '@/api/projects';
import Avatar from '@/components/Avatar';
import BackButton from '@/components/Button/BackButton';
import RatingOverview from '@/components/RatingOverview';
import { mapPercentToLetterRating } from '@/helpers';
import { formatDate } from '@/helpers/date';
import { convertToBase64 } from '@/helpers/logo';
import { Meta } from '@/layouts/Meta';
import { Main } from '@/templates/Main';

function ProjectPage({ id }: any) {
  const [project, setProject]: any = useState({});
  const letter = mapPercentToLetterRating(project.ratings);

  async function fetchProjectById(projectId: any) {
    try {
      const { data: updatedProject } = await getProjectById(projectId);
      setProject(updatedProject);
    } catch (error: any) {
      toast.error(error?.message);
    }
  }

  useEffect(() => {
    if (id) {
      fetchProjectById(id);
    }
  }, [id]);

  function tweet() {
    const path = window.location.href;
    const text = `Check out ${project.name}'s score on #PrimeRating @PrimeDAO_`;

    window.open(
      `http://twitter.com/share?url=${encodeURIComponent(
        path
      )}&text=${encodeURIComponent(text)}`,
      'twitsharer',
      'toolbar=0,status=0,width=626,height=436'
    );
    return false;
  }

  return (
    <Main meta={<Meta title="Projects" description="" />}>
      {project && (
        <section className="my-6 mx-auto max-w-screen-lg rounded-xl bg-white p-2 text-xs md:p-8 md:text-base">
          <article className={'mb-12 flex flex-col md:flex-row'}>
            <div className={'grow'}>
              <div className="mb-10 flex items-center">
                <Avatar
                  width={56}
                  height={56}
                  src={convertToBase64(project.logo)}
                  placeholder="/website/assets/placeholders/project.png"
                  className={'mr-6'}
                />
                <h3
                  className={
                    'text-xl font-bold capitalize leading-none text-black md:text-4xl'
                  }
                >
                  {project.name}
                </h3>
              </div>
              <div className={'mb-8 flex max-w-xl justify-between text-black'}>
                <div>
                  <h3 className={'text-sm text-gray-800'}>Category</h3>
                  <p className={'font-bold'}>
                    {project.categories?.join(', ')}
                  </p>
                </div>
                <div>
                  <h3 className={'text-sm text-gray-800'}>Project site</h3>
                  <p>
                    {project.projectUrl ? (
                      <Link href={project.projectUrl}>
                        <a
                          target={'_blank'}
                          className={`border-none font-bold text-[#8668fc] hover:text-[#ff497a]`}
                        >
                          {project.projectUrl}
                        </a>
                      </Link>
                    ) : (
                      <span>—</span>
                    )}
                  </p>
                </div>
                <div>
                  <h3 className={'text-sm text-gray-800'}>Last updated</h3>
                  <p className={'font-bold'}>
                    {formatDate(project.lastUpdated) || '—'}
                  </p>
                </div>
              </div>
              <div className={'text-gray-600'}>&nbsp;</div>
            </div>
            <div className="basis-full rounded-md text-center md:basis-1/5">
              <div
                className={`score-card inline-block min-w-[50px] rounded border ${letter?.toLowerCase()} p-3 text-center text-yellow-500 md:p-6`}
              >
                <div className={'mb-1'}>
                  <div>Prime Rating</div>
                  <p className="text-2xl font-bold">
                    {letter || '—'} | {project.percentage}
                  </p>
                </div>
                {project.ratings?.map((rating: any, index: number) => (
                  <div className={'mb-1'} key={index}>
                    <div>{rating.reportType}</div>
                    <p className={'text-xl font-bold'}>{rating.percentage}%</p>
                  </div>
                ))}
              </div>
            </div>
          </article>

          <article
            className={
              'mb-6 flex flex-col space-x-0 space-y-8 md:flex-row md:space-y-0 md:space-x-8'
            }
          >
            {project.ratings?.map((rating: any, index: number) => {
              const { reportType } = rating;
              const report = project.reports
                .filter((item: any) => item.type === reportType)
                .sort(
                  (item: any, nextItem: any) =>
                    // @ts-ignore
                    new Date(item.acceptedAt) - new Date(nextItem.acceptedAt)
                )
                .pop();

              return (
                <RatingOverview key={index} rating={rating} report={report} />
              );
            })}
          </article>

          <div className={'mt-10 flex justify-between'}>
            <BackButton />
            <button
              onClick={tweet}
              className={
                'flex rounded-3xl bg-gray-200 p-3 px-5 text-xs uppercase text-gray-500 hover:bg-[#efebf0]'
              }
            >
              <svg
                className="mr-2 h-5 w-5"
                fill="currentColor"
                viewBox="0 0 24 24"
                aria-hidden="true"
              >
                <path d="M8.29 20.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0022 5.92a8.19 8.19 0 01-2.357.646 4.118 4.118 0 001.804-2.27 8.224 8.224 0 01-2.605.996 4.107 4.107 0 00-6.993 3.743 11.65 11.65 0 01-8.457-4.287 4.106 4.106 0 001.27 5.477A4.072 4.072 0 012.8 9.713v.052a4.105 4.105 0 003.292 4.022 4.095 4.095 0 01-1.853.07 4.108 4.108 0 003.834 2.85A8.233 8.233 0 012 18.407a11.616 11.616 0 006.29 1.84"></path>
              </svg>
              share on twitter
            </button>
          </div>
        </section>
      )}
    </Main>
  );
}

export async function getStaticPaths() {
  return {
    paths: ['/projects/1'],
    fallback: true,
  };
}

export async function getStaticProps({ params }: any) {
  return {
    props: { id: params.id },
  };
}

// export async function getServerSideProps() {
//   return { props: {} };
// }

export default ProjectPage;
