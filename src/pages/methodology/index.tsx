import 'react-medium-image-zoom/dist/styles.css';

import Link from 'next/link';
import Zoom from 'react-medium-image-zoom';

import RatingBadge from '@/components/RatingBadge';
import { Table, THead } from '@/components/Table';
import { Meta } from '@/layouts/Meta';
import { Main } from '@/templates/Main';

const Methodology = () => (
  <Main meta={<Meta title="Methodology" description="" />}>
    <section
      className={
        'content rounded-2xl bg-white p-5 text-base text-gray-600 md:p-10'
      }
    >
      <h2
        className={
          'mb-10 border-b bg-gradient-to-tr from-[#a258a7] to-[#ff497a] bg-clip-text text-5xl font-bold text-transparent'
        }
      >
        Methodology
      </h2>
      <h3
        className={
          'text-xl font-bold leading-6 text-black md:text-2xl md:leading-8'
        }
      >
        Rating Framework(s) + Open Sourcing Data + Permissionless Governance
        Process = fair, verifiable, and unbiased rating scores
      </h3>
      <p>
        The rating app is a consolidated overview of all protocols that have
        been reviewed by Prime raters. The benchmark (A+ to D) is calculated by
        using the average of both technical and fundamental report scores
        (50/50), whereas the fundamental score for instance, can consist of
        multiple reports for one and the same protocol. Allowing numerous raters
        to review the same protocols increases veracity and ensures the scores
        are kept up-to-date. The App also provides a list of raters that have
        contributed to Prime Rating and lets anyone transparently review the
        reports that make up the rating scores.
      </p>
      <hr className={'mb-8'} />
      <h3 className={'text-2xl font-bold leading-8 text-black'}>
        Prime Rating Scale
      </h3>
      <p>
        To provide a simple and comparable opinion, protocols are rated on a
        scale from A+ to D depending on the aggregated outcome of the technical
        (smart contract quality) and fundamental review(value proposition,
        tokenomics, governance, etc.).
      </p>

      <Table>
        <THead>
          <tr>
            <th className={'w-24 font-bold md:w-28'}>Rating</th>
            <th className={'font-bold'}>Description</th>
            <th className={'font-bold'}>Score</th>
          </tr>
        </THead>
        <tbody>
          <tr>
            <td className={'py-2'}>
              <RatingBadge rating={'A+'} />
            </td>
            <td className={'py-2'}>Prime-grade protocol</td>
            <td className={'py-2'}>
              Average score &gt; 85% &amp; Min. score &gt; 70%
            </td>
          </tr>
          <tr>
            <td className={'py-2'}>
              <RatingBadge rating={'A'} />
            </td>
            <td className={'py-2'}>High-grade protocol</td>
            <td className={'py-2'}>
              Average score &gt; 75% &amp; Min. score &gt; 60%
            </td>
          </tr>
          <tr>
            <td className={'py-2'}>
              <RatingBadge rating={'B+'} />
            </td>
            <td className={'py-2'}>Upper medium-grade protocol</td>
            <td className={'py-2'}>
              Average score &gt; 65% &amp; Min. score &gt; 50%
            </td>
          </tr>
          <tr>
            <td className={'py-2'}>
              <RatingBadge rating={'B'} />
            </td>
            <td className={'py-2'}>Medium-grade protocol</td>
            <td className={'py-2'}>
              Average score &gt; 55% &amp; Min. score &gt; 40%
            </td>
          </tr>
          <tr>
            <td className={'py-2'}>
              <RatingBadge rating={'C+'} />
            </td>
            <td className={'py-2'}>Lower medium-grade protocol</td>
            <td className={'py-2'}>
              Average score &gt; 45% &amp; Min. score &gt; 30%
            </td>
          </tr>
          <tr>
            <td className={'py-2'}>
              <RatingBadge rating={'C'} />
            </td>
            <td className={'py-2'}>Low-grade protocol</td>
            <td className={'py-2'}>Average score &gt; 35%</td>
          </tr>
          <tr>
            <td className={'py-2'}>
              <RatingBadge rating={'D'} />
            </td>
            <td className={'py-2'}>Protocol not rateable </td>
            <td className={'py-2'}>Average score &lt; 35%</td>
          </tr>
        </tbody>
      </Table>

      <hr className={'mb-8'} />

      <h3 className={'text-2xl font-bold leading-8 text-black'}>
        Permissionless Rating
      </h3>

      <p>
        To provide a neutral and all-encompassing opinion on open finance
        protocols, Prime Rating has developed a{' '}
        <a
          target={'_blank'}
          className={
            'border-none font-bold text-[#8668fc] hover:text-[#ff497a]'
          }
          href="https://primedao.gitbook.io/prime-rating/prime-rating-squad/framework-overview"
          rel="noreferrer"
        >
          permissionless and modular framework
        </a>{' '}
        that utilizes DAO best practices and decentralized data storage to
        operate as independently from any centralized entity as technologically
        possible. By being permissionless, Prime Rating is more resilient,
        neutral, and able to leverage the wisdom of the crowd. All Prime Ratings
        and documents are publicly available and can be accessed non-exclusively
        by anyone with an internet connection, making Prime Rating a public good
        for the benefit of the systemic advancement of open finance.
      </p>

      <Zoom>
        <img
          src="/assets/content/methodology/diagram.png"
          loading="lazy"
          srcSet="/assets/content/methodology/diagram-500.png 500w, /assets/content/methodology/diagram-800.png 800w, /assets/content/methodology/diagram-1080.png 1080w, /assets/content/methodology/diagram-1400.png 1400w"
          // sizes="(max-width: 479px) 100vw, (max-width: 767px) 79vw, (max-width: 991px) 80vw, 1400px"
          alt=""
          className="mx-auto w-full"
        />
      </Zoom>

      <ul>
        <li>1. Prime Rating provides report templates</li>
        <li>
          2. Raters evaluate a protocol using either a fundamental or technical
          report template
        </li>
        <li>3. Once ready, raters can ask for feedback on their report</li>
        <li>
          4. After implementing feedback, raters can submit the report which
          triggers a Snapshot vote
        </li>
        <li>
          5. A council of experienced raters (200+ RXP) vote on whether to
          accept or reject a report
        </li>
        <li>
          6. If accepted, the reports gets added to the DB and the score is
          updated accordingly
        </li>
        <li>
          7. If rejected, the rater can improve the report and submit again
          (only two consecutive submissions are allowed)
        </li>
        <li>
          8. As a reward for successfully submitting a report, a rater will
          receive 150 USDC + 200 $D2D (minimum) plus rating experience points
          (RXP)
        </li>
        <li>
          9. Raters that consistently create high quality reports can level-up
          and increase their rewards, become reviewers, become eligible for
          voting and receive other benefits
        </li>
      </ul>

      <hr className={'mb-8'} />

      <h3 className={'text-2xl font-bold leading-8 text-black'}>
        Prime Rating Building Blocks
      </h3>

      <p>The following building blocks make Prime Rating possible:</p>
      <p>
        <b>Prime Rating Backend (Beta version):</b> Prime Rating&apos;s backend
        is built in a trustless manner using IPFS to store all reports (in
        .pdf). In its final state, the backend will function fully automated and
        without any human interaction, making the framework fully permissionless
        and trustless, only governed by the DAO.
      </p>
      <p>
        <b>Report Templates:</b> Prime Rating relies on a set of report
        templates accessible to anyone with an internet connection. These
        templates are the standard format through which rating scores are
        created and updated. The templates are improved on a regular basis and
        also governed by the DAO.
      </p>
      <p>
        <b>Prime Rating Governance:</b> Prime Rating Governance is at the core
        of the framework and responsible for accepting/rejecting reports, thus
        ensuring high quality AND veracity of the rating scores.
      </p>
      <p>
        <b>Prime Community:</b> The community is at the heart of the DAO and has
        a voice in all important matters. Decision making is done through
        discussion to find consensus and finally via voting.
      </p>
      <p>
        <b>Communication Channels:</b> Discord is used for communication.
        PrimeDAO Discord uses Collabland to allow different tiers of Prime
        Raters to autonomously claim their Rank in the PrimeDAO Discord Channel.
      </p>
      <hr className={'mb-8'} />

      <h3 className={'text-2xl font-bold leading-8 text-black'}>
        Technical Rating
      </h3>
      <p>
        The technical report facilitates reviewing protocols based on best
        practices and adherence to overall process transparency. The process
        quality review is an independent review system that rates the
        documentation, software quality, access controls, reading the audit(s),
        and developer&apos;s practices. This report does not constitute an
        audit, but rather a complementary report to aid in technical evaluation
        of protocols.
      </p>
      <p>
        <Link href="https://docs.defisafety.com/review-process-documentation/process-quality-audit-process">
          <a
            target={'_blank'}
            className={
              'rounded-full border border-transparent bg-[#ff497a] py-3 px-4 text-sm font-bold uppercase text-white hover:border-pink-100 hover:bg-white hover:text-pink-400 md:text-base'
            }
          >
            VIEW TECHNICAL REPORT TEMPLATE
          </a>
        </Link>
      </p>
      <hr className={'mb-8'} />

      <h3 className={'text-2xl font-bold leading-8 text-black'}>
        Fundamental Rating
      </h3>
      <p>
        The fundamental report enables a comprehensive evaluation of a
        protocol&apos;s overall quality and uncovers potential risks. The report
        is a tool to perform a deep analysis of the value offering, usefulness,
        organizational strengths/weaknesses, and differentiating factors such as
        competitive advantages vs. other protocols in the space. Together with
        the technical rating, the fundamental report provides an overarching
        scan of what a protocol has to offer. ‍ The fundamental report consists
        of five main sections, each evaluating a different angle of a protocol,
        these are: Value proposition, tokenomics, team, governance and
        regulatory.
      </p>
      <p>
        <Link href="https://docs.google.com/document/d/1v9TIwB99PgJwuJu4u3tqZ-qxTeQYAi6kJRTQvTUDxR4/edit#heading=h.gjdgxs">
          <a
            target={'_blank'}
            className={
              'rounded-full border border-transparent bg-[#ff497a] py-3 px-4 text-sm font-bold uppercase text-white hover:border-pink-100 hover:bg-white hover:text-pink-400 md:text-base'
            }
          >
            VIEW FUNDAMENTAL REPORT TEMPLATE
          </a>
        </Link>
      </p>
      <div className={'mt-10'}>
        <Link href="/rating-defi">
          <a className={'border-none font-semibold text-gray-500'}>
            &larr; Back to Prime Rating
          </a>
        </Link>
      </div>
    </section>
  </Main>
);

export default Methodology;
