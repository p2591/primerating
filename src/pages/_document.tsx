import Document, { Head, Html, Main, NextScript } from 'next/document';

import { Footer } from '@/layouts/Footer';
import { Header } from '@/templates/Header';
import { AppConfig } from '@/utils/AppConfig';

// Need to create a custom _document because i18n support is not compatible with `next export`.
class MyDocument extends Document {
  // eslint-disable-next-line class-methods-use-this
  render() {
    return (
      <Html lang={AppConfig.locale}>
        <Head />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Changa+One:400,400italic%7CLato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic%7CInter:100,200,300,regular,500,600,700,800,900&display=optional"
        />
        <body className="bg-[#f9f6f9]">
          <Header />
          <Main />
          <Footer />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
