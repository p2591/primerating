import slugify from 'slugify';

function RankBadge({ value }) {
  if (!value) return null;

  const slugifiedValue = slugify(value, { lower: true });

  return (
    <div
      className={`rank-badge inline-block min-w-[50px] rounded border px-2 py-0 text-center text-base ${slugifiedValue}`}
    >
      {value}
    </div>
  );
}

export default RankBadge;
