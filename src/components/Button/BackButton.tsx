import { useRouter } from 'next/router';

function BackButton() {
  const router = useRouter();

  return (
    <button
      onClick={() => router.back()}
      className={'font-semibold text-gray-500 hover:text-red-400'}
    >
      ← Back
    </button>
  );
}
export default BackButton;
