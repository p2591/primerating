import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useState } from 'react';

import { getProjects, SECTORS } from '@/api/projects';
import Avatar from '@/components/Avatar';
import RatingBadge from '@/components/RatingBadge';
import { isEqual, mapPercentToLetterRating } from '@/helpers';
import { formatDate } from '@/helpers/date';
import { convertToBase64 } from '@/helpers/logo';

function NewListings() {
  const router = useRouter();
  const [prevParams, setPrevParams]: any = useState();
  const [newListings, setNewListings] = useState([]);

  const fetchData = useCallback(async () => {
    const { sector: newSector, ...params } = router.query;
    try {
      let sector = newSector;

      if (
        newSector === SECTORS.TECHNICAL ||
        newSector === SECTORS.FUNDAMENTAL
      ) {
        sector = SECTORS.DEFI;
      }

      const { data: updatedData } = await getProjects(sector, {
        ...params,
        page: 0,
        size: 3,
        sort: 'lastUpdated,desc',
      });

      return updatedData;
    } catch (error) {
      return { content: [] };
    }
  }, [router.query]);

  useEffect(() => {
    if (!prevParams || !isEqual(prevParams, router.query)) {
      fetchData().then((data) => setNewListings(data?.content));
      setPrevParams(router.query);
    }
  }, [prevParams, router.query]);

  return (
    <article className={'flex flex-col'}>
      <h3 className="mb-2 text-sm uppercase text-gray-600">New listings</h3>
      <div className="grow basis-1/2 rounded-xl bg-white py-2.5 text-sm">
        <ul className="m-0 divide-y p-0">
          {newListings.map(
            ({ id, name, logo = '', lastUpdated, ratings = [] }) => {
              const letter = mapPercentToLetterRating(ratings);

              return (
                <li key={id}>
                  <Link href={`/projects/${id}`}>
                    <a
                      className={
                        'flex items-center space-x-3 border-none py-2 px-2.5 text-black hover:bg-gray-200'
                      }
                    >
                      <div className="w-3/5">
                        <Avatar
                          src={convertToBase64(logo)}
                          placeholder="/website/assets/placeholders/project.png"
                        />
                        <span>{name}</span>
                      </div>
                      <div className={'flex grow items-center justify-between'}>
                        <div>
                          <RatingBadge size={'sm'} rating={letter} />
                        </div>
                        <div
                          className={
                            ' min-w-[95px] py-1 text-right text-gray-600'
                          }
                        >
                          {formatDate(lastUpdated)}
                        </div>
                      </div>
                    </a>
                  </Link>
                </li>
              );
            }
          )}
        </ul>
      </div>
    </article>
  );
}

export default NewListings;
