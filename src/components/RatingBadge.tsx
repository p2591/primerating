const RATINGS = [
  {
    rating: 'A+',
    classes: 'bg-[#eef7ee] text-[#4cb050] border-[rgb(76,176,80)]/30',
  },
  {
    rating: 'A',
    classes: 'bg-[#f3f9ed] text-[#8cc34b] border-[rgb(140,195,75)]/30',
  },
  {
    rating: 'B+',
    classes:
      'bg-[rgb(205,220,57)]/10 text-[#cddc39] border-[rgb(205,220,57)]/30',
  },
  {
    rating: 'B',
    classes:
      'bg-[rgb(255,236,58)]/10 text-[#daca31] border-[rgb(218,202,49)]/30',
  },
  {
    rating: 'C+',
    classes: 'bg-[rgb(255,194,8)]/10 text-[#ffc208] border-[rgb(255,194,8)]/30',
  },
  {
    rating: 'C',
    classes: 'bg-[rgb(255,152,1)]/10 text-[#ff9801] border-[rgb(255,152,1)]/30',
  },
  {
    rating: 'D',
    classes: 'bg-[rgb(254,87,34)]/10 text-[#fe5722] border-[rgb(254,87,34)]/30',
  },
];

type SIZES = 'sm' | 'xs' | 'base';

function RatingBadge({
  size,
  rating = '',
  score,
  className = '',
}: {
  rating: string;
  size?: SIZES;
  score?: number;
  className?: string;
}) {
  const ratingObject = RATINGS.find((item) => {
    return item.rating.toUpperCase() === rating.toUpperCase();
  });

  const ratingClasses = ratingObject?.classes || 'bg-green-50 text-orange-400';
  const padding =
    // eslint-disable-next-line no-nested-ternary
    size === 'xs' ? 'py-0' : size === 'base' ? 'px-3 py-1' : 'px-1 py-[2px]';

  return rating ? (
    <div
      className={`inline-flex ${
        size !== 'sm' && size !== 'xs' ? '' : 'min-w-[40px]'
      } items-center justify-center rounded border text-center ${ratingClasses} ${padding} ${className}`}
    >
      <span className={'w-[25px] px-1 text-center font-bold'}>
        {rating.toUpperCase()}
      </span>
      {score && (
        <>
          <span className={'mx-0.5 text-gray-400'}>|</span>
          <span className={'w-[25px] px-1 text-center'}>{score}</span>
        </>
      )}
    </div>
  ) : null;
}

export default RatingBadge;
