import Link from 'next/link';

import Avatar from '@/components/Avatar';
import { formatDate } from '@/helpers/date';
import { convertToBase64 } from '@/helpers/logo';

const REPORT_TYPES = {
  TECHNICAL: 'technical',
  FUNDAMENTAL: 'fundamental',
};

function Report({ report }: any) {
  const { type, ipfsHash, acceptedAt, authors = [], website } = report;
  const user = authors?.[0] ?? {};
  const link =
    type === REPORT_TYPES.TECHNICAL
      ? website
      : `https://primerating.mypinata.cloud/ipfs/${ipfsHash}`;

  return (
    <div className="mt-5 text-sm text-gray-600 md:mt-10">
      <h4 className={'mb-2 text-gray-600 md:mb-0'}>Rated by</h4>
      <div className={'flex flex-col justify-between md:flex-row '}>
        <div className={'mb-3 md:mb-0'}>
          <Link href={`/raters/${user.id}`}>
            <a
              className={
                'flex items-center border-none text-black hover:text-[#ff497a]'
              }
            >
              <Avatar
                className={'mr-2'}
                src={convertToBase64(user.logo)}
                placeholder="/website/assets/placeholders/rater.png"
              />
              <div>
                <div className={'font-bold'}>{user.name}</div>
                <p className={''}>{user.rank}</p>
              </div>
            </a>
          </Link>
        </div>
        <div>
          <Link href={link}>
            <a
              className={
                'flex border-none text-sm font-bold text-black hover:text-[#ff497a] md:text-base'
              }
              target={'_blank'}
            >
              View report
              <img
                className={'ml-2'}
                src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTMiIGhlaWdodD0iMTMiIHZpZXdCb3g9IjAgMCAxMyAxMyIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTEyLjY1NjkgMTEuNjU2OUwxMC42NjI4IDExLjY1NjlMMTAuNjU1NyAzLjc1ODQ4TDIuMDUwMjUgMTIuMzY0TDAuNjM2MDM4IDEwLjk0OThMOS4yNDE1MyAyLjM0NDI3TDEuMzQzMTQgMi4zMzcyTDEuMzQzMTQgMC4zNDMxNTlMMTIuNjU2OSAwLjM0MzE1N0wxMi42NTY5IDExLjY1NjlaIiBmaWxsPSIjRkY0OTdBIi8+Cjwvc3ZnPgo="
                alt=""
              />
            </a>
          </Link>
          <p>{formatDate(acceptedAt)}</p>
        </div>
      </div>
    </div>
  );
}

function RatingOverview({ rating, report }: any) {
  const { id, percentage, reportType, sections = [] } = rating;

  return (
    <div
      key={id}
      className="mx-auto flex w-full flex-col justify-between rounded-lg bg-[#f9f6f9] p-2 md:w-1/2 md:p-4"
    >
      <div>
        <h3 className="mb-2 flex justify-between text-xl font-bold capitalize text-[#ff497a]">
          {reportType} <span>{percentage && `${percentage}%`}</span>
        </h3>
        {sections.map((section: any) => {
          return (
            <div
              key={section.name}
              className={
                'flex justify-between border-none py-0 align-middle text-base text-gray-600 md:py-1'
              }
            >
              <div>{section.label}</div>
              <div className={'py-1 text-right'}>{section.percentage}%</div>
            </div>
          );
        })}
      </div>
      {report && <Report report={report} />}
    </div>
  );
}

export default RatingOverview;
