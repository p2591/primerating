import Link from 'next/link';

import ad from '@/public/assets/ads/rating-metaverse.png';

function UpdatesAd() {
  return (
    <article className={'flex min-h-[192px] flex-col'}>
      <h3 className="mb-2 text-sm uppercase text-gray-600">Updates</h3>
      <div className="flex grow basis-1/2 rounded-xl bg-white text-sm">
        <Link href="https://medium.com/primedao/kickstarting-the-first-refi-rate-athon-c5394c671fe6">
          <a
            className={`h-full grow basis-1/2 rounded-xl border-none bg-cover bg-center bg-no-repeat p-2.5 text-sm`}
            style={{ backgroundImage: `url(${ad.src})` }}
          />
        </Link>
      </div>
    </article>
  );
}

export default UpdatesAd;
