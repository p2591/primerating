import { useState } from 'react';

function Tabs({ children, initialActive = '' }: any) {
  const [active, setActive] = useState(initialActive);

  return (
    <div
      className={`flex shrink-0 border-b border-b-gray-400 text-base text-gray-600`}
    >
      {children({ active, setActive })}
    </div>
  );
}

export function Tab({ onClick, active = false, children }: any) {
  return (
    <button
      onClick={onClick}
      className={`border-b p-1 px-2 md:p-3 md:px-6 ${
        active ? 'text-red-600' : ''
      } ${active ? 'border-b-red-500' : 'border-b-gray-400'}`}
    >
      {children}
    </button>
  );
}

export default Tabs;
