import { useRouter } from 'next/router';

import Avatar from '@/components/Avatar';
import Row from '@/components/ContentTables/Row';
import RankBadge from '@/components/RankBadge';
import { convertToBase64 } from '@/helpers/logo';

function Item({ item, rowIndex }: any) {
  const router = useRouter();
  const { id, twitter, name, logo, rank, rxp } = item;
  return (
    <Row key={id} onClick={() => router.push(`/raters/${id}`)}>
      <td className="w-[50px] min-w-[50px] p-2 pl-5">{rowIndex + 1}</td>
      <td className=" w-[200px] min-w-[200px] grow items-center py-1 text-black">
        <Avatar
          src={convertToBase64(logo)}
          placeholder="/website/assets/placeholders/rater.png"
        />
        <span className={'whitespace-nowrap border-none text-black'}>
          {name}
        </span>
      </td>
      <td className="min-w-[80px] py-2 px-1 text-center">
        <div className={'divide-x'}>
          <RankBadge value={rank} />
        </div>
      </td>
      <td className="py-2 px-1 text-center">{rxp}</td>
      <td className="py-2 px-1 text-center">{twitter ? `@${twitter}` : ''}</td>
    </Row>
  );
}
export default Item;
