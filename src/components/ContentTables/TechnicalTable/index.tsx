import { Table } from '@/components/Table';

import Item from './Item';

function TechnicalTable({ size, number, data }: any) {
  return (
    <Table>
      <thead className="sticky-border-bottom relative top-0  bg-white md:sticky md:top-36">
        <tr className={'px-4 text-sm font-semibold md:px-5'}>
          <td className="w-[40px] min-w-[40px] p-2 pl-5">#</td>
          <td className="w-[150px] min-w-[150px] p-2">Protocol</td>
          <td className="p-2 text-center">Technical score</td>
          <td className="p-2 text-center">Testing</td>
          <td className="p-2 text-center">Documentation</td>
          <td className="w-[90px] min-w-[90px] p-2 text-center">
            Smart Contracts & Team
          </td>
          <td className="p-2 text-center">Oracles</td>
          <td className="p-2 text-center">Admin Controls</td>
          <td className="p-2 text-center">Security</td>
        </tr>
      </thead>
      <tbody>
        {data.map((item: any, index: number) => {
          const rowIndex = number * size + index;

          return <Item key={item.id} rowIndex={rowIndex} item={item} />;
        })}
      </tbody>
    </Table>
  );
}

export default TechnicalTable;
