import { Table } from '@/components/Table';

import Item from './Item';

function SectorTable({ size, number, data }: any) {
  return (
    <Table>
      <thead className="sticky-border-bottom relative top-0  bg-white md:sticky md:top-36">
        <tr className={'px-4 text-sm font-semibold md:px-5'}>
          <td className="w-[40px] min-w-[40px] p-2 pl-5">#</td>
          <td className="min-w-[270px] p-2">Protocol</td>
          <td className="min-w-[150px] p-2 text-center">Overall rating</td>
          <td className="p-2 text-center">Technical</td>
          <td className="p-2 text-center">Fundamental</td>
          <td className="p-2 text-center">Category</td>
          <td className="p-2 text-center">Last updated</td>
        </tr>
      </thead>
      <tbody>
        {data.map((item: any, index: number) => {
          const rowIndex = number * size + index;

          return <Item key={item.id} rowIndex={rowIndex} item={item} />;
        })}
      </tbody>
    </Table>
  );
}

export default SectorTable;
