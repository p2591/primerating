import { useRouter } from 'next/router';

import Avatar from '@/components/Avatar';
import Row from '@/components/ContentTables/Row';
import RatingBadge from '@/components/RatingBadge';
import { mapPercentToLetterRating } from '@/helpers';
import { formatDate } from '@/helpers/date';
import { convertToBase64 } from '@/helpers/logo';

function Item({ item, rowIndex }: any) {
  const {
    id,
    name,
    percentage,
    logo,
    categories = [],
    lastUpdated,
    ratings = [],
  } = item;
  const router = useRouter();
  const letter = mapPercentToLetterRating(ratings);

  const fundamental = Array.isArray(ratings)
    ? ratings.find(({ reportType }) => reportType === 'fundamental')
    : null;
  const technical = Array.isArray(ratings)
    ? ratings.find(({ reportType }) => reportType === 'technical')
    : null;

  return (
    <Row key={id} onClick={() => router.push(`/projects/${id}`)}>
      <td className="w-[50px] min-w-[50px] p-2 pl-5">{rowIndex + 1}</td>
      <td className="grow items-center py-3 text-black">
        <Avatar
          src={convertToBase64(logo)}
          placeholder="/website/assets/placeholders/project.png"
        />
        <span className={'whitespace-nowrap border-none text-black'}>
          {name}
        </span>
      </td>
      <td className="min-w-[85px] py-2 px-1 text-center">
        <div className={'divide-x'}>
          <RatingBadge rating={letter} score={percentage} />
        </div>
      </td>
      <td className="py-2 px-1 text-center">
        {technical ? `${technical.percentage}%` : '—'}
      </td>
      <td className="py-2 px-1 text-center">
        {fundamental ? `${fundamental.percentage}%` : '—'}
      </td>
      <td className="py-2 px-1 text-center">{categories.join(', ')}</td>
      <td className="min-w-[130px] py-2 px-1 text-center">
        {formatDate(lastUpdated)}
      </td>
    </Row>
  );
}
export default Item;
