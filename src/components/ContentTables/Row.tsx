import { noop } from '@/helpers';

function Row({ onClick = noop, children }: any) {
  return (
    <tr
      onClick={onClick}
      className={
        'border-b border-gray-200 text-[15px] text-gray-600 hover:cursor-pointer hover:bg-gray-100'
      }
    >
      {children}
    </tr>
  );
}

export default Row;
