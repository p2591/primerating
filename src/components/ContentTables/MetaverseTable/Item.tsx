import { useRouter } from 'next/router';
import { useMemo } from 'react';

import Avatar from '@/components/Avatar';
import Row from '@/components/ContentTables/Row';
import RatingBadge from '@/components/RatingBadge';
import { formatDate } from '@/helpers/date';
import { convertToBase64 } from '@/helpers/logo';

const ORDER = [
  'valueProposition',
  'competitiveMoat',
  'tokenEconomics',
  'team',
  'governance',
  'inGameEconomy',
];

function sortInSpecialOrder(a: any, b: any) {
  return ORDER.indexOf(a.name) - ORDER.indexOf(b.name);
  // for the sake of recent versions of Google Chrome use:
  // return a.key.charCodeAt(0) > b.key.charCodeAt(0); or return a.key.charCodeAt(0) - b.key.charCodeAt(0);
}

function Item({ item, rowIndex }: any) {
  const router = useRouter();
  const { id, name, logo, ratings, letter, lastUpdated } = item;
  const { percentage, sections: unsortedSections = [] } = ratings?.[0] || {};

  const sortedSections = useMemo(
    () =>
      unsortedSections
        .filter(({ name: sectorName }: any) => ORDER.indexOf(sectorName) > -1)
        .sort(sortInSpecialOrder)
        .slice(0, ORDER.length),
    [item]
  );

  // length can be different. Make it the same
  const sections = [
    ...sortedSections,
    ...(Array.from({ length: ORDER.length - sortedSections.length }) || []),
  ];

  return (
    <Row key={id} onClick={() => router.push(`/projects/${id}`)}>
      <td className="w-[50px] min-w-[50px] p-2 pl-5">{rowIndex + 1}</td>
      <td className="max-w-[150px] grow items-center overflow-hidden text-ellipsis whitespace-nowrap py-1 text-black">
        <Avatar
          src={convertToBase64(logo)}
          placeholder="/website/assets/placeholders/project.png"
        />
        <span className={'whitespace-nowrap border-none text-black'}>
          {name}
        </span>
      </td>
      <td className="py-2 px-1 text-center">
        <div className={'divide-x'}>
          <RatingBadge rating={letter} score={percentage} />
        </div>
      </td>
      {sections.map((section: any, index) => (
        <td key={section?.name || index} className="py-2 px-1 text-center">
          {section ? `${section.percentage}%` : '—'}
        </td>
      ))}
      <td className="min-w-[130px] py-2 px-1 text-center">
        {formatDate(lastUpdated)}
      </td>
    </Row>
  );
}
export default Item;
