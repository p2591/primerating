import Cookies from 'js-cookie';
import { useEffect, useState } from 'react';

function Cookie() {
  const [hidden, setHidden] = useState(true);

  useEffect(() => {
    setHidden(!!Cookies.get('fs-cc'));
  }, []);

  function bannerHandler() {
    Cookies.set('fs-cc', {
      id: 'P5jQ-cvc07jir2uaF4R4c',
      consents: {
        analytics: true,
        essential: true,
        marketing: true,
        personalization: true,
        uncategorized: true,
      },
    });

    setHidden(true);
  }

  if (hidden) {
    return null;
  }

  return (
    <article
      className={
        'fixed bottom-1 right-1 z-10 ml-1 flex max-w-xl space-x-3 rounded-md border bg-white p-4 text-xs text-gray-600'
      }
    >
      <div>
        <p>
          This website uses cookies for functionality, analytics and advertising
          purposes as described in our{' '}
          <a
            className={
              'border-none font-bold text-[#8668fc] hover:text-[#ff497a]'
            }
            target={'_blank'}
            href={`/assets/pdf/cookie-policy.pdf`}
            rel="noreferrer"
          >
            Privacy Policy
          </a>
          . If you agree to our use of cookies, please continue to use our site.
        </p>
      </div>
      <div className={'self-center'}>
        <button
          onClick={bannerHandler}
          className={
            'rounded border border-black bg-white py-2 px-3 text-base font-bold uppercase text-black hover:border-[#ff497a] hover:text-[#ff497a]'
          }
        >
          ok
        </button>
      </div>
    </article>
  );
}

export default Cookie;
