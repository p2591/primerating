function THead({ children, className = '' }) {
  return (
    <thead className={`text-xs font-semibold ${className}`}>{children}</thead>
  );
}

export default THead;
