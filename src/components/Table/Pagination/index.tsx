import { createFilledArray } from '@/helpers';

function Pagination({ current, total, onClick: onPageChange }) {
  const totalIndex = total - 1;

  const pagesArr =
    totalIndex <= 10
      ? createFilledArray(total)
      : [1, 2, null, totalIndex, totalIndex + 1];

  return (
    <div className="flex justify-center">
      <button
        disabled={current === 0}
        onClick={() => onPageChange(current - 1)}
        className={`mx-1 flex ${
          current === 0 ? 'cursor-default text-gray-300' : 'text-gray-700'
        } items-center justify-center rounded-md border-none bg-white px-4 py-2 capitalize`}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-5 w-5"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
            clipRule="evenodd"
          />
        </svg>
      </button>

      {pagesArr.map((pageNumber, index) => {
        const pageIndex = pageNumber - 1;

        if (!pageNumber) {
          return (
            <span
              key={index}
              className="mx-1 hidden rounded-md bg-white px-4 py-2 text-gray-700 transition-colors duration-200 sm:inline"
            >
              ...
            </span>
          );
        }
        return (
          <button
            key={`page-${pageNumber}`}
            onClick={() =>
              current === pageIndex ? null : onPageChange(pageIndex)
            }
            className={`mx-1 ${
              current === pageIndex ? 'cursor-default bg-gray-200' : ''
            } hidden rounded-md border-none bg-white px-4 py-2 text-gray-700 transition-colors duration-200 sm:inline`}
          >
            {pageNumber}
          </button>
        );
      })}

      <button
        disabled={current >= totalIndex}
        onClick={() => onPageChange(current + 1)}
        className={`mx-1 ${
          current >= totalIndex
            ? 'cursor-default text-gray-300'
            : 'text-gray-700'
        } flex items-center justify-center rounded-md border-none bg-white px-4 py-2 text-gray-700 transition-colors duration-200`}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-5 w-5"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
            clipRule="evenodd"
          />
        </svg>
      </button>
    </div>
  );
}

export default Pagination;
