function Table({ children }: any) {
  return (
    <table className="w-full table-auto text-left  text-sm text-black">
      {children}
    </table>
  );
}

export default Table;
