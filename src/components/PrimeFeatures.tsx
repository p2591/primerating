import Link from 'next/link';
import { useRouter } from 'next/router';

function PrimeFeatures() {
  const router = useRouter();

  return (
    <article className={'bg-white py-10 md:py-24'}>
      <div className="mx-auto max-w-screen-lg">
        <div className={'mb-10 text-center md:mb-20'}>
          <h3 className="mb-4 bg-gradient-to-bl from-[#a258a7] to-[#ff497a] bg-clip-text text-2xl font-bold leading-tight text-transparent md:text-5xl">
            More from Prime
          </h3>
          <p className={'text-lg'}>
            Prime offers a full set of relational tools for digital
            organisations.
          </p>
        </div>
        <div className={'space-y-6 md:columns-3 md:gap-x-4'}>
          <Link href={'/launch'}>
            <div
              className={
                'relative top-0  h-full cursor-pointer rounded-2xl border bg-white p-7 py-10 text-black shadow transition duration-300 ease-in-out hover:-translate-y-2 hover:border-red-600 hover:text-red-500'
              }
            >
              <img
                className={'absolute right-3 top-3'}
                src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0wIDBIMjRWMjRIMFYwWiIgc3Ryb2tlPSJibGFjayIgc3Ryb2tlLW9wYWNpdHk9IjAuMDExNzY0NyIgc3Ryb2tlLXdpZHRoPSIwIi8+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNNy40NjQ1IDcuMDUwMjVDNy40NjQ0OSA2LjQ5Nzk4IDcuOTEyMjMgNi4wNTAyNCA4LjQ2NDQ5IDYuMDUwMjZMMTYuOTQ5OCA2LjA1MDI2QzE3LjIxNDkgNi4wNTAyNiAxNy40Njk0IDYuMTU1NjIgMTcuNjU2OSA2LjM0MzE1QzE3Ljg0NDQgNi41MzA2NyAxNy45NDk4IDYuNzg1MDkgMTcuOTQ5OCA3LjA1MDI1TDE3Ljk0OTggMTUuNTM1NUMxNy45NDk4IDE2LjA4NzggMTcuNTAyIDE2LjUzNTUgMTYuOTQ5OCAxNi41MzU1QzE2LjM5NzUgMTYuNTM1NSAxNS45NDk4IDE2LjA4NzggMTUuOTQ5OCAxNS41MzU1TDE1Ljk0OTggOS40NjQ0Nkw3Ljc1NzM5IDE3LjY1NjlDNy4zNjY4NyAxOC4wNDc0IDYuNzMzNzEgMTguMDQ3NCA2LjM0MzE4IDE3LjY1NjlDNS45NTI2NCAxNy4yNjYzIDUuOTUyNjYgMTYuNjMzMiA2LjM0MzE4IDE2LjI0MjZMMTQuNTM1NiA4LjA1MDI0TDguNDY0NTEgOC4wNTAyNUM3LjkxMjIzIDguMDUwMjMgNy40NjQ1MiA3LjYwMjUyIDcuNDY0NSA3LjA1MDI1WiIgZmlsbD0iI0ZGNDk3QSIvPgo8L3N2Zz4K"
                alt=""
              />
              <img
                src={`${router.basePath}/assets/images/launch.svg`}
                alt=""
                className="mb-2 w-11"
              />
              <h4 className={'mb-1 text-xl'}>
                <b>Prime</b> Launch
              </h4>
              <p className={'text-sm'}>
                A decentralized token offering platform powered by Balancer V
                2.0 that allows for fair and user-friendly token launches.
              </p>
            </div>
          </Link>
          <Link href={'/pools'}>
            <div
              className={
                'relative top-0  h-full cursor-pointer rounded-2xl border bg-white p-7 py-10 text-black shadow transition duration-300 ease-in-out hover:-translate-y-2 hover:border-red-600 hover:text-red-500'
              }
            >
              <div>
                <img
                  className={'absolute right-3 top-3'}
                  src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0wIDBIMjRWMjRIMFYwWiIgc3Ryb2tlPSJibGFjayIgc3Ryb2tlLW9wYWNpdHk9IjAuMDExNzY0NyIgc3Ryb2tlLXdpZHRoPSIwIi8+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNNy40NjQ1IDcuMDUwMjVDNy40NjQ0OSA2LjQ5Nzk4IDcuOTEyMjMgNi4wNTAyNCA4LjQ2NDQ5IDYuMDUwMjZMMTYuOTQ5OCA2LjA1MDI2QzE3LjIxNDkgNi4wNTAyNiAxNy40Njk0IDYuMTU1NjIgMTcuNjU2OSA2LjM0MzE1QzE3Ljg0NDQgNi41MzA2NyAxNy45NDk4IDYuNzg1MDkgMTcuOTQ5OCA3LjA1MDI1TDE3Ljk0OTggMTUuNTM1NUMxNy45NDk4IDE2LjA4NzggMTcuNTAyIDE2LjUzNTUgMTYuOTQ5OCAxNi41MzU1QzE2LjM5NzUgMTYuNTM1NSAxNS45NDk4IDE2LjA4NzggMTUuOTQ5OCAxNS41MzU1TDE1Ljk0OTggOS40NjQ0Nkw3Ljc1NzM5IDE3LjY1NjlDNy4zNjY4NyAxOC4wNDc0IDYuNzMzNzEgMTguMDQ3NCA2LjM0MzE4IDE3LjY1NjlDNS45NTI2NCAxNy4yNjYzIDUuOTUyNjYgMTYuNjMzMiA2LjM0MzE4IDE2LjI0MjZMMTQuNTM1NiA4LjA1MDI0TDguNDY0NTEgOC4wNTAyNUM3LjkxMjIzIDguMDUwMjMgNy40NjQ1MiA3LjYwMjUyIDcuNDY0NSA3LjA1MDI1WiIgZmlsbD0iI0ZGNDk3QSIvPgo8L3N2Zz4K"
                  alt=""
                />
                <img
                  src={`${router.basePath}/assets/images/pools.svg`}
                  alt=""
                  className="mb-2 w-11"
                />
                <h4 className={'mb-1 text-xl'}>
                  <b>Prime</b> Pools
                </h4>
                <p className={'text-sm'}>
                  DAO-governed liquidity pools that bridge liquidity between
                  allied ecosystems and create index pools.
                </p>
              </div>
            </div>
          </Link>
          <Link href={'/deals'}>
            <div
              className={
                'relative top-0  h-full cursor-pointer rounded-2xl border bg-white p-7 py-10 text-black shadow transition duration-300 ease-in-out hover:-translate-y-2 hover:border-red-600 hover:text-red-500'
              }
            >
              <div>
                <img
                  className={'absolute right-3 top-3'}
                  src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0wIDBIMjRWMjRIMFYwWiIgc3Ryb2tlPSJibGFjayIgc3Ryb2tlLW9wYWNpdHk9IjAuMDExNzY0NyIgc3Ryb2tlLXdpZHRoPSIwIi8+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNNy40NjQ1IDcuMDUwMjVDNy40NjQ0OSA2LjQ5Nzk4IDcuOTEyMjMgNi4wNTAyNCA4LjQ2NDQ5IDYuMDUwMjZMMTYuOTQ5OCA2LjA1MDI2QzE3LjIxNDkgNi4wNTAyNiAxNy40Njk0IDYuMTU1NjIgMTcuNjU2OSA2LjM0MzE1QzE3Ljg0NDQgNi41MzA2NyAxNy45NDk4IDYuNzg1MDkgMTcuOTQ5OCA3LjA1MDI1TDE3Ljk0OTggMTUuNTM1NUMxNy45NDk4IDE2LjA4NzggMTcuNTAyIDE2LjUzNTUgMTYuOTQ5OCAxNi41MzU1QzE2LjM5NzUgMTYuNTM1NSAxNS45NDk4IDE2LjA4NzggMTUuOTQ5OCAxNS41MzU1TDE1Ljk0OTggOS40NjQ0Nkw3Ljc1NzM5IDE3LjY1NjlDNy4zNjY4NyAxOC4wNDc0IDYuNzMzNzEgMTguMDQ3NCA2LjM0MzE4IDE3LjY1NjlDNS45NTI2NCAxNy4yNjYzIDUuOTUyNjYgMTYuNjMzMiA2LjM0MzE4IDE2LjI0MjZMMTQuNTM1NiA4LjA1MDI0TDguNDY0NTEgOC4wNTAyNUM3LjkxMjIzIDguMDUwMjMgNy40NjQ1MiA3LjYwMjUyIDcuNDY0NSA3LjA1MDI1WiIgZmlsbD0iI0ZGNDk3QSIvPgo8L3N2Zz4K"
                  alt=""
                />
                <img
                  src={`${router.basePath}/assets/images/deals.svg`}
                  alt=""
                  className="mb-2 w-11"
                />
                <h4 className={'mb-1 text-xl'}>
                  <b>Prime</b> Deals
                </h4>
                <p className={'text-sm'}>
                  Interface for DAO to DAO interactions, such as token swaps,
                  co-liquidity provision, and joint venture formation.
                </p>
              </div>
            </div>
          </Link>
        </div>
      </div>
    </article>
  );
}

export default PrimeFeatures;
