function ChevronRight({ className = '' }) {
  return (
    <span className={`h-4 w-2 ${className} inline-flex`}>
      <svg
        fill="currentColor"
        viewBox="0 0 46.001 85.999"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="m1.003 80.094c-1.338 1.352-1.338 3.541 0 4.893 1.337 1.35 3.506 1.352 4.845 0l39.149-39.539c1.338-1.352 1.338-3.543 0-4.895l-39.149-39.539c-1.339-1.352-3.506-1.352-4.845 0-1.338 1.352-1.338 3.541-.001 4.893l35.704 37.093z" />
      </svg>
    </span>
  );
}

export default ChevronRight;
