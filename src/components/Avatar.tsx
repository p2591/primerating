function Avatar({
  className = '',
  alt = '',
  src = '',
  placeholder = '',
  ...props
}) {
  if (!src) {
    src = placeholder;
  }

  return (
    <img
      src={src}
      className={`mr-3 inline-block h-6 w-6 rounded-full bg-gray-500 ring-2 ring-gray-300 ${className}`}
      alt={alt}
      {...props}
    />
  );
}

export default Avatar;
