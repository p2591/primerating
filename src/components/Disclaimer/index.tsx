import Link from 'next/link';

function Disclaimer() {
  return (
    <article className="mb-0 md:mb-10">
      <div
        className={
          'mx-auto flex max-w-screen-md flex-col md:flex-row md:space-x-3'
        }
      >
        <div className={'p-2 text-xs text-gray-600 md:p-0'}>
          All content on this site is for informational and educational purposes
          only, you should not construe any such information or other material
          as legal, tax, investment, financial, or other advice.
        </div>
        <div
          className={
            'mt-1 mb-5 flex items-center justify-center md:mb-2 md:w-2/3'
          }
        >
          <Link
            href={`https://docs.prime.xyz/prime-rating/prime-rating-squad/disclaimer`}
          >
            <a
              target={'_blank'}
              className={`rounded-full border-2 border-none bg-white px-4 py-2 text-sm font-bold capitalize text-red-400 hover:bg-[#ff497a] hover:text-white`}
            >
              Read full disclaimer
            </a>
          </Link>
        </div>
      </div>
    </article>
  );
}

export default Disclaimer;
