function SearchInput({ onChange, className = '' }: any) {
  return (
    <input
      type="search"
      placeholder="Search"
      onChange={(e: any) => {
        if (onChange) {
          onChange(e.target.value);
        }
      }}
      className={`rounded bg-gray-200 bg-[url("data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHZpZXdCb3g9IjAgMCAxOCAxOCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTEyLjUgMTEuNUgxMS43MUwxMS40MyAxMS4yM0MxMi40MSAxMC4wOSAxMyA4LjYxIDEzIDdDMTMgMy40MSAxMC4wOSAwLjUgNi41IDAuNUMyLjkxIDAuNSAwIDMuNDEgMCA3QzAgMTAuNTkgMi45MSAxMy41IDYuNSAxMy41QzguMTEgMTMuNSA5LjU5IDEyLjkxIDEwLjczIDExLjkzTDExIDEyLjIxVjEzTDE2IDE3Ljk5TDE3LjQ5IDE2LjVMMTIuNSAxMS41Wk02LjUgMTEuNUM0LjAxIDExLjUgMiA5LjQ5IDIgN0MyIDQuNTEgNC4wMSAyLjUgNi41IDIuNUM4Ljk5IDIuNSAxMSA0LjUxIDExIDdDMTEgOS40OSA4Ljk5IDExLjUgNi41IDExLjVaIiBmaWxsPSIjRkY0OTdBIi8+Cjwvc3ZnPgo=")] bg-[right_calc(10px)_top_calc(50%)] bg-no-repeat px-7 py-2 text-sm placeholder:text-gray-600 lg:w-80 ${className}`}
    />
  );
}
export default SearchInput;
