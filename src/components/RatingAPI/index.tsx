import Link from 'next/link';

import chevron from '@/public/assets/icons/chevron-right-red.svg';

function RatingAPI() {
  return (
    <article className={'flex flex-col'}>
      <h3 className="mb-2 text-sm uppercase text-gray-600">Api</h3>
      <div className="grow basis-1/2 rounded-xl bg-white py-2.5 px-4 text-sm">
        <h4 className="mt-0 mb-5 bg-gradient-to-tr from-[#a258a7] to-[#ff497a] bg-clip-text text-2xl font-bold text-transparent">
          Rating API
        </h4>
        <p className={'mb-2 text-gray-500'}>
          Register to get early-access to the Prime Rating API
        </p>
        <Link href={'https://primedao.typeform.com/ratingapi'}>
          <a
            target={'_blank'}
            className="inline-flex items-center border-none bg-right bg-no-repeat pr-4 font-semibold uppercase text-gray-500 transition-all ease-in-out hover:pr-5 hover:text-red-500"
            style={{
              backgroundImage: `url(${chevron.src})`,
            }}
          >
            <span className="">Register</span>
          </a>
        </Link>
      </div>
    </article>
  );
}

export default RatingAPI;
