import api, { API_URL } from '@/api/index';

export function getRaters(params = {}) {
  return api.get(`${API_URL}/raters`, { ...params, sort: 'percentage,desc' });
}

export function getRaterById(id: any) {
  return api.get(`${API_URL}/raterPage/${id}`);
}

export function searchRaters(name: string, params: any = {}) {
  const { page = 0, size = 100 } = params;

  return api.get(`${API_URL}/raters?name=${name}`, {
    page,
    size,
    sort: 'name,desc',
  });
}
