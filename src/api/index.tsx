import axios from 'axios';

export const API_URL = `https://api.prime.xyz`;

const headers = {
  'x-api-key':
    'a4ea41e9f50759f8396a59a97b7351b861e072cfeb0a56e48f98871f0e40dee7',
};

async function get(endpoint, params = {}) {
  const strParams = new URLSearchParams(params).toString();

  return axios.get(`${endpoint}?${strParams}`, {
    headers,
  });
}

function post(endpoint, payload) {
  return axios.post(endpoint, payload, { headers });
}

const api = { get, post };

export default api;
