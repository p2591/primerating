import api, { API_URL } from '@/api/index';

export const SECTORS = {
  DEFI: 'defi',
  METAVERSE: 'metaverse',
  REFI: 'refi',
  TECHNICAL: 'technical',
  FUNDAMENTAL: 'fundamental',
};

function getProjectsBySector(sector: any, params: any = {}) {
  const { page = 0, size = 100 } = params;

  return api.get(`${API_URL}/projectsBySector/${sector}`, {
    page,
    size,
    sort: 'percentage,desc',
  });
}

function getProjectsByType(type: any, params: any = {}) {
  const { page = 0, size = 100 } = params;

  return api.get(`${API_URL}/projectsByType/${type}`, {
    page,
    size,
    sort: 'percentage,desc',
  });
}

export function getProjects(sector: any = SECTORS.DEFI, params: any = {}) {
  if (sector === SECTORS.TECHNICAL || sector === SECTORS.FUNDAMENTAL) {
    return getProjectsByType(sector, params);
  }
  return getProjectsBySector(sector, params);
}

export function getProjectById(id: any) {
  return api.get(`${API_URL}/projectPage/${id}`);
}

export function searchProjects(sector: string, name: string, params: any = {}) {
  const { page = 0, size = 100 } = params;

  return api.get(`${API_URL}/projects/search/${sector}/${name}`, {
    page,
    size,
    sort: 'name,desc',
  });
}
