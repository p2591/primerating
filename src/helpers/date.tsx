// Jun 10, 2022
type IOptions = {
  month?: any;
  day?: any;
  year?: any;
};

const DEFAULT_OPTIONS = {
  month: 'short',
  day: 'numeric',
  year: 'numeric',
};

export function formatDate(
  date: any,
  locale = 'en-US',
  options?: IOptions
): any {
  if (!date) return '';

  return new Date(date).toLocaleDateString(locale, {
    ...DEFAULT_OPTIONS,
    ...options,
  });
}
