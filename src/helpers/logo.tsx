export function convertToBase64(logo: string): string {
  if (!logo) {
    return '';
  }
  console.log(logo);
  return `data:image/png;base64, ${logo}`;
}
