export function createFilledArray(total: any) {
  return (
    Array(total)
      // @ts-ignore
      .fill()
      // @ts-ignore
      .map((e: any, i: number) => i + 1)
  );
}

export function isEqual(obj1: any, obj2: any) {
  return (
    Object.keys(obj1).length === Object.keys(obj2).length &&
    Object.keys(obj1).every((key) => obj1[key] === obj2[key])
  );
}

/**
 * @description
 * Takes an Array<V>, and a grouping function,
 * and returns a Map of the array grouped by the grouping function.
 *
 * @param list An array of type V.
 * @param keyGetter A Function that takes the the Array type V as an input, and returns a value of type K.
 *                  K is generally intended to be a property key of V.
 *
 * @returns Map of the array grouped by the grouping function.
 */
// export function groupBy<K, V>(list: Array<V>, keyGetter: (input: V) => K): Map<K, Array<V>> {
//    const map = new Map<K, Array<V>>();
export function groupBy(list: any, keyGetter: any) {
  const map = new Map();
  list.forEach((item: any) => {
    const key = keyGetter(item);
    const collection = map.get(key);
    if (!collection) {
      map.set(key, [item]);
    } else {
      collection.push(item);
    }
  });
  return map;
}

export function mapPercentToLetterRating(ratings: any = []) {
  const RATING_MAP = {
    version: '0.2',
    lettering: [
      {
        letter: 'A+',
        minScore: 70,
        averageScore: 85,
      },
      {
        letter: 'A',
        minScore: 60,
        averageScore: 75,
      },
      {
        letter: 'B+',
        minScore: 50,
        averageScore: 65,
      },
      {
        letter: 'B',
        minScore: 40,
        averageScore: 55,
      },
      {
        letter: 'C+',
        minScore: 30,
        averageScore: 45,
      },
      {
        letter: 'C',
        minScore: 0,
        averageScore: 35,
      },
      {
        letter: 'D',
        minScore: 0,
        averageScore: 0,
      },
      {
        letter: '-',
        minScore: -1,
        averageScore: -1,
      },
    ],
  };

  if (ratings.length < 2) {
    return '';
  }

  const [rating1, rating2] = ratings;
  const averagePercentage = (rating1.percentage + rating2.percentage) / 2;
  const ratingScore = RATING_MAP.lettering.find(
    (obj) =>
      rating1.percentage > obj.minScore &&
      rating2.percentage > obj.minScore &&
      averagePercentage > obj.averageScore
  );

  return ratingScore?.letter || '';
}

export function noop() {}
