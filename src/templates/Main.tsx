import type { ReactNode } from 'react';

import PrimeFeatures from '@/components/PrimeFeatures';

type IMainProps = {
  meta: ReactNode;
  children: ReactNode;
};

const Main = (props: IMainProps) => (
  <div className="w-full px-1 text-gray-700 antialiased">
    {props.meta}

    <div className="mx-auto max-w-screen-lg py-10">
      <div className="py-5 text-xl">{props.children}</div>
    </div>
    <PrimeFeatures />
  </div>
);

export { Main };
