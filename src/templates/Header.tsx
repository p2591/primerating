import Link from 'next/link';

import ChevronRight from '@/components/Icons/ChevronRight';

function Nav({ children }: any) {
  return (
    <div className="my-2 flex grow justify-end space-x-7 md:my-0 md:space-x-10">
      {children}
    </div>
  );
}

Nav.Link = function NavLink({ href, children, ...props }: any) {
  return (
    <Link href={href}>
      <a
        {...props}
        className="text-base  text-black hover:border-none hover:text-red-500"
      >
        {children}
      </a>
    </Link>
  );
};

function Header() {
  return (
    <div className="sticky top-0 z-10 bg-white px-4 sm:px-6">
      <div className="mx-auto flex flex-col items-center justify-end py-1 md:max-w-6xl md:flex-row md:space-x-10 md:py-3">
        <div className="flex w-28 md:w-40">
          <Link href="/">
            <a className="border-none">
              <span className="sr-only">Prime Rating</span>
              <img
                src={'/website/assets/images/logotype.svg'}
                width="165"
                height="56"
                alt="Prime Rating"
              />
            </a>
          </Link>
        </div>
        <Nav>
          <Nav.Link href="/methodology">Methodology</Nav.Link>
          <Nav.Link href="/raters">Raters</Nav.Link>
          <Nav.Link href="/about">About</Nav.Link>
          <Nav.Link href="https://www.prime.xyz">PrimeDAO</Nav.Link>
        </Nav>
        <div className="my-2 items-center md:my-0">
          <a
            target={'_blank'}
            href="https://docs.prime.xyz/prime-rating/products/permissionless-rating"
            className="ml-8 flex items-center justify-center whitespace-nowrap rounded-full border-none bg-gradient-to-tr from-[#8668fc] to-[#a258a7] px-6 py-1 text-base uppercase text-white shadow-sm hover:border-none hover:bg-indigo-400 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
            rel="noreferrer"
          >
            <span className="mx-1">Become a rater</span>
            <ChevronRight className={'ml-3'} />
          </a>
        </div>
      </div>
    </div>
  );
}

export { Header };
