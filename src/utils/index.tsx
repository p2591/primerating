export const throttle = (callback: any, delay: any) => {
  let timeoutHandler: any = null;

  return function t(...args: any) {
    if (timeoutHandler == null) {
      timeoutHandler = setTimeout(() => {
        callback(...args);
        clearInterval(timeoutHandler);
        timeoutHandler = null;
      }, delay);
    }
  };
};
